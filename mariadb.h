/* Copyright (C) 2000 MySQL AB & MySQL Finland AB & TCX DataKonsult AB
                 2012 by MontyProgram AB

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02111-1301, USA */

/* defines for the libmariadb library */

typedef char my_bool;
typedef unsigned long long my_ulonglong;
typedef int my_socket;

/*
** Common definition between mysql server & client
*/

static const int NAME_CHAR_LEN = 64;
static const int NAME_LEN = 256;		/* Field/table name length */
static const int HOSTNAME_LENGTH = 255;
static const int SYSTEM_MB_MAX_CHAR_LENGTH = 4;
static const int USERNAME_CHAR_LENGTH = 128;
static const int USERNAME_LENGTH = (USERNAME_CHAR_LENGTH * SYSTEM_MB_MAX_CHAR_LENGTH);
static const int SERVER_VERSION_LENGTH = 60;
static const int SQLSTATE_LENGTH = 5;
static const int SCRAMBLE_LENGTH = 20;
static const int SCRAMBLE_LENGTH_323 = 8;

enum mysql_enum_shutdown_level
{
  SHUTDOWN_DEFAULT = 0,
  KILL_QUERY= 254,
  KILL_CONNECTION= 255
};

enum enum_server_command
{
  COM_SLEEP = 0,
  COM_QUIT,
  COM_INIT_DB,
  COM_QUERY,
  COM_FIELD_LIST,
  COM_CREATE_DB,
  COM_DROP_DB,
  COM_REFRESH,
  COM_SHUTDOWN,
  COM_STATISTICS,
  COM_PROCESS_INFO,
  COM_CONNECT,
  COM_PROCESS_KILL,
  COM_DEBUG,
  COM_PING,
  COM_TIME = 15,
  COM_DELAYED_INSERT,
  COM_CHANGE_USER,
  COM_BINLOG_DUMP,
  COM_TABLE_DUMP,
  COM_CONNECT_OUT = 20,
  COM_REGISTER_SLAVE,
  COM_STMT_PREPARE = 22,
  COM_STMT_EXECUTE = 23,
  COM_STMT_SEND_LONG_DATA = 24,
  COM_STMT_CLOSE = 25,
  COM_STMT_RESET = 26,
  COM_SET_OPTION = 27,
  COM_STMT_FETCH = 28,
  COM_DAEMON= 29,
  COM_UNSUPPORTED= 30,
  COM_RESET_CONNECTION = 31,
  COM_STMT_BULK_EXECUTE = 250,
  COM_RESERVED_1 = 254, /* former COM_MULTI, now removed */
  COM_END
};


static const int NOT_NULL_FLAG = 1;		/* Field can't be NULL */
static const int PRI_KEY_FLAG = 2;		/* Field is part of a primary key */
static const int UNIQUE_KEY_FLAG = 4;		/* Field is part of a unique key */
static const int MULTIPLE_KEY_FLAG = 8;		/* Field is part of a key */
static const int BLOB_FLAG = 16;		/* Field is a blob */
static const int UNSIGNED_FLAG = 32;		/* Field is unsigned */
static const int ZEROFILL_FLAG = 64;		/* Field is zerofill */
static const int BINARY_FLAG = 128;
/* The following are only sent to new clients */
static const int ENUM_FLAG = 256;		/* field is an enum */
static const int AUTO_INCREMENT_FLAG = 512;		/* field is a autoincrement field */
static const int TIMESTAMP_FLAG =1024;		/* Field is a timestamp */
static const int SET_FLAG = 2048;		/* field is a set */
/* new since 3.23.58 */
static const int NO_DEFAULT_VALUE_FLAG = 4096;	/* Field doesn't have default value */
static const int ON_UPDATE_NOW_FLAG = 8192;         /* Field is set to NOW on UPDATE */
/* end new */
static const int NUM_FLAG = 32768;		/* Field is num (for clients) */
static const int PART_KEY_FLAG = 16384;		/* Intern; Part of some key */
static const int GROUP_FLAG = 32768;		/* Intern: Group field */
static const int UNIQUE_FLAG = 65536;		/* Intern: Used by sql_yacc */

static const int REFRESH_GRANT = 1;	/* Refresh grant tables */
static const int REFRESH_LOG = 2;	/* Start on new log file */
static const int REFRESH_TABLES = 4;	/* close all tables */
static const int REFRESH_HOSTS = 8;	/* Flush host cache */
static const int REFRESH_STATUS = 16;	/* Flush status variables */
static const int REFRESH_THREADS = 32;	/* Flush thread cache */
static const int REFRESH_SLAVE = 64;      /* Reset master info and restart slave
					   thread */
static const int REFRESH_MASTER = 128;     /* Remove all bin logs in the index
					   and truncate the index */

/* The following can't be set with mysql_refresh() */
static const int REFRESH_READ_LOCK = 16384;	/* Lock tables for read */
static const int REFRESH_FAST = 32768;	/* Intern flag */

static const unsigned int CLIENT_MYSQL = 1;
static const unsigned int CLIENT_FOUND_ROWS = 2;	/* Found instead of affected rows */
static const unsigned int CLIENT_LONG_FLAG = 4;	/* Get all column flags */
static const unsigned int CLIENT_CONNECT_WITH_DB = 8;	/* One can specify db on connect */
static const unsigned int CLIENT_NO_SCHEMA = 16;	/* Don't allow database.table.column */
static const unsigned int CLIENT_COMPRESS = 32;	/* Can use compression protocol */
static const unsigned int CLIENT_ODBC = 64;	/* Odbc client */
static const unsigned int CLIENT_LOCAL_FILES = 128;	/* Can use LOAD DATA LOCAL */
static const unsigned int CLIENT_IGNORE_SPACE = 256; /* Ignore spaces before '(' */
static const unsigned int CLIENT_INTERACTIVE = 1024;	/* This is an interactive client */
static const unsigned int CLIENT_SSL = 2048;     /* Switch to SSL after handshake */
static const unsigned int CLIENT_IGNORE_SIGPIPE = 4096;     /* IGNORE sigpipes */
static const unsigned int CLIENT_TRANSACTIONS = 8192;	/* Client knows about transactions */
/* added in 4.x */
static const unsigned int CLIENT_PROTOCOL_41 = 512;
static const unsigned int CLIENT_RESERVED = 16384;
static const unsigned int CLIENT_SECURE_CONNECTION = 32768;
static const unsigned int CLIENT_MULTI_STATEMENTS = (1U << 16);
static const unsigned int CLIENT_MULTI_RESULTS = (1U << 17);
static const unsigned int CLIENT_PS_MULTI_RESULTS = (1U << 18);
static const unsigned int CLIENT_PLUGIN_AUTH = (1U << 19);
static const unsigned int CLIENT_CONNECT_ATTRS = (1U << 20);
static const unsigned int CLIENT_PLUGIN_AUTH_LENENC_CLIENT_DATA = (1U << 21);
static const unsigned int CLIENT_CAN_HANDLE_EXPIRED_PASSWORDS = (1U << 22);
static const unsigned int CLIENT_SESSION_TRACKING = (1U << 23);
static const unsigned int CLIENT_ZSTD_COMPRESSION = (1U << 26);
static const unsigned int CLIENT_PROGRESS = (1U << 29); /* client supports progress indicator */
static const unsigned int CLIENT_PROGRESS_OBSOLETE = CLIENT_PROGRESS;
static const unsigned int CLIENT_SSL_VERIFY_SERVER_CERT = (1U << 30);
static const unsigned int CLIENT_REMEMBER_OPTIONS = (1U << 31);

static const int MYSQL_ERRMSG_SIZE = 512;

struct st_ma_pvio;
typedef struct st_ma_pvio MARIADB_PVIO;

struct st_ma_connection_plugin;

typedef struct st_net {
  MARIADB_PVIO *pvio;
  unsigned char *buff;
  unsigned char *buff_end,*write_pos,*read_pos;
  my_socket fd;					/* For Perl DBI/dbd */
  unsigned long remain_in_buf,length;
  unsigned long buf_length, where_b;
  unsigned long max_packet, max_packet_size;
  unsigned int pkt_nr, compress_pkt_nr;
  unsigned int write_timeout, read_timeout, retry_count;
  int fcntl;
  unsigned int *return_status;
  unsigned char reading_or_writing;
  char save_char;
  char unused_1;
  my_bool unused_2;
  my_bool compress;
  my_bool unused_3;
  void *unused_4;
  unsigned int last_errno;
  unsigned char error;
  my_bool unused_5;
  my_bool unused_6;
  char last_error[MYSQL_ERRMSG_SIZE];
  char sqlstate[SQLSTATE_LENGTH+1];
  struct st_mariadb_net_extension *extension;
} NET;

static const unsigned int packet_error = ((unsigned int) -1);

/* used by mysql_set_server_option */
enum enum_mysql_set_option
{
  MYSQL_OPTION_MULTI_STATEMENTS_ON,
  MYSQL_OPTION_MULTI_STATEMENTS_OFF
};

/* for status callback function */
enum enum_mariadb_status_info
{
  STATUS_TYPE= 0,
  SESSION_TRACK_TYPE
};

enum enum_session_state_type
{
  SESSION_TRACK_SYSTEM_VARIABLES= 0,
  SESSION_TRACK_SCHEMA,
  SESSION_TRACK_STATE_CHANGE,
  /* currently not supported by MariaDB Server */
  SESSION_TRACK_GTIDS,
  SESSION_TRACK_TRANSACTION_CHARACTERISTICS,
  SESSION_TRACK_TRANSACTION_STATE /* make sure that SESSION_TRACK_END always points
                                    to last element of enum !! */
};

static const int SESSION_TRACK_BEGIN = 0;
static const int SESSION_TRACK_END = SESSION_TRACK_TRANSACTION_STATE;
static const int SESSION_TRACK_TYPES = (SESSION_TRACK_END + 1);

/* SESSION_TRACK_TRANSACTION_TYPE was renamed to SESSION_TRACK_TRANSACTION_STATE
   in 3e699a1738cdfb0a2c5b8eabfa8301b8d11cf711.
   This is a workaround to prevent breaking of travis and buildbot tests.
   TODO: Remove this after server fixes */
static const int SESSION_TRACK_TRANSACTION_TYPE = SESSION_TRACK_TRANSACTION_STATE;

enum enum_field_types { MYSQL_TYPE_DECIMAL, MYSQL_TYPE_TINY,
                        MYSQL_TYPE_SHORT,  MYSQL_TYPE_LONG,
                        MYSQL_TYPE_FLOAT,  MYSQL_TYPE_DOUBLE,
                        MYSQL_TYPE_NULL,   MYSQL_TYPE_TIMESTAMP,
                        MYSQL_TYPE_LONGLONG,MYSQL_TYPE_INT24,
                        MYSQL_TYPE_DATE,   MYSQL_TYPE_TIME,
                        MYSQL_TYPE_DATETIME, MYSQL_TYPE_YEAR,
                        MYSQL_TYPE_NEWDATE, MYSQL_TYPE_VARCHAR,
                        MYSQL_TYPE_BIT,
                        /*
                          the following types are not used by client,
                          only for mysqlbinlog!!
                        */
                        MYSQL_TYPE_TIMESTAMP2,
                        MYSQL_TYPE_DATETIME2,
                        MYSQL_TYPE_TIME2,
                        /* --------------------------------------------- */
                        MYSQL_TYPE_JSON=245,
                        MYSQL_TYPE_NEWDECIMAL=246,
                        MYSQL_TYPE_ENUM=247,
                        MYSQL_TYPE_SET=248,
                        MYSQL_TYPE_TINY_BLOB=249,
                        MYSQL_TYPE_MEDIUM_BLOB=250,
                        MYSQL_TYPE_LONG_BLOB=251,
                        MYSQL_TYPE_BLOB=252,
                        MYSQL_TYPE_VAR_STRING=253,
                        MYSQL_TYPE_STRING=254,
                        MYSQL_TYPE_GEOMETRY=255,
                        MAX_NO_FIELD_TYPES };

static const int FIELD_TYPE_DECIMAL = MYSQL_TYPE_DECIMAL;
static const int FIELD_TYPE_NEWDECIMAL = MYSQL_TYPE_NEWDECIMAL;
static const int FIELD_TYPE_TINY = MYSQL_TYPE_TINY;
static const int FIELD_TYPE_CHAR = FIELD_TYPE_TINY;   /* For compatibility */
static const int FIELD_TYPE_SHORT = MYSQL_TYPE_SHORT;
static const int FIELD_TYPE_LONG = MYSQL_TYPE_LONG;
static const int FIELD_TYPE_FLOAT = MYSQL_TYPE_FLOAT;
static const int FIELD_TYPE_DOUBLE = MYSQL_TYPE_DOUBLE;
static const int FIELD_TYPE_NULL = MYSQL_TYPE_NULL;
static const int FIELD_TYPE_TIMESTAMP = MYSQL_TYPE_TIMESTAMP;
static const int FIELD_TYPE_LONGLONG = MYSQL_TYPE_LONGLONG;
static const int FIELD_TYPE_INT24 = MYSQL_TYPE_INT24;
static const int FIELD_TYPE_DATE = MYSQL_TYPE_DATE;
static const int FIELD_TYPE_TIME = MYSQL_TYPE_TIME;
static const int FIELD_TYPE_DATETIME = MYSQL_TYPE_DATETIME;
static const int FIELD_TYPE_YEAR = MYSQL_TYPE_YEAR;
static const int FIELD_TYPE_NEWDATE = MYSQL_TYPE_NEWDATE;
static const int FIELD_TYPE_ENUM = MYSQL_TYPE_ENUM;
static const int FIELD_TYPE_INTERVAL = FIELD_TYPE_ENUM; /* For compatibility */
static const int FIELD_TYPE_SET = MYSQL_TYPE_SET;
static const int FIELD_TYPE_TINY_BLOB = MYSQL_TYPE_TINY_BLOB;
static const int FIELD_TYPE_MEDIUM_BLOB = MYSQL_TYPE_MEDIUM_BLOB;
static const int FIELD_TYPE_LONG_BLOB = MYSQL_TYPE_LONG_BLOB;
static const int FIELD_TYPE_BLOB = MYSQL_TYPE_BLOB;
static const int FIELD_TYPE_VAR_STRING = MYSQL_TYPE_VAR_STRING;
static const int FIELD_TYPE_STRING = MYSQL_TYPE_STRING;
static const int FIELD_TYPE_GEOMETRY = MYSQL_TYPE_GEOMETRY;
static const int FIELD_TYPE_BIT = MYSQL_TYPE_BIT;

extern unsigned long max_allowed_packet;
extern unsigned long net_buffer_length;

int	ma_net_init(NET *net, MARIADB_PVIO *pvio);
void	ma_net_end(NET *net);
void	ma_net_clear(NET *net);
int	ma_net_flush(NET *net);
int	ma_net_write(NET *net,const unsigned char *packet, size_t len);
int	ma_net_write_command(NET *net,unsigned char command,const char *packet,
			  size_t len, my_bool disable_flush);
int	ma_net_real_write(NET *net,const char *packet, size_t len);
extern unsigned long ma_net_read(NET *net);

struct rand_struct {
  unsigned long seed1,seed2,max_value;
  double max_value_dbl;
};

  /* The following is for user defined functions */

enum Item_result {STRING_RESULT,REAL_RESULT,INT_RESULT,ROW_RESULT,DECIMAL_RESULT};

typedef struct st_udf_args
{
  unsigned int arg_count;		/* Number of arguments */
  enum Item_result *arg_type;		/* Pointer to item_results */
  char **args;				/* Pointer to argument */
  unsigned long *lengths;		/* Length of string arguments */
  char *maybe_null;			/* Set to 1 for all maybe_null args */
} UDF_ARGS;

  /* This holds information about the result */

typedef struct st_udf_init
{
  my_bool maybe_null;			/* 1 if function can return NULL */
  unsigned int decimals;		/* for real functions */
  unsigned int max_length;		/* For string functions */
  char	  *ptr;				/* free pointer for function data */
  my_bool const_item;			/* 0 if result is independent of arguments */
} UDF_INIT;

/* Connection types */
static const int MARIADB_CONNECTION_UNIXSOCKET = 0;
static const int MARIADB_CONNECTION_TCP        = 1;
static const int MARIADB_CONNECTION_NAMEDPIPE  = 2;
static const int MARIADB_CONNECTION_SHAREDMEM  = 3;

  /* Constants when using compression */
static const int NET_HEADER_SIZE = 4;		/* standard header size */
static const int COMP_HEADER_SIZE = 3;		/* compression header extra size */

char *ma_scramble_323(char *to,const char *message,const char *password);
void ma_scramble_41(const unsigned char *buffer, const char *scramble, const char *password);
void ma_hash_password(unsigned long *result, const char *password, size_t len);
void ma_make_scrambled_password(char *to,const char *password);

/* Some other useful functions */

void mariadb_load_defaults(const char *conf_file, const char **groups,
		   int *argc, char ***argv);
my_bool ma_thread_init(void);
void ma_thread_end(void);

static const unsigned int NULL_LENGTH = ((unsigned int)~0); /* For net_store_length */

typedef struct st_list {
  struct st_list *prev,*next;
  void *data;
} LIST;

typedef int (*list_walk_action)(void *,void *);

extern LIST *list_add(LIST *root,LIST *element);
extern LIST *list_delete(LIST *root,LIST *element);
extern LIST *list_cons(void *data,LIST *root);
extern LIST *list_reverse(LIST *root);
extern void list_free(LIST *root,unsigned int free_data);
extern unsigned int list_length(LIST *list);
extern int list_walk(LIST *list,list_walk_action action,char * argument);

/*
  A better implementation of the UNIX ctype(3) library.
  Notes:   my_global.h should be included before ctype.h
*/

static const int MY_CS_NAME_SIZE = 32;

/* we use the mysqlnd implementation */
typedef struct ma_charset_info_st
{
  unsigned int	nr; /* so far only 1 byte for charset */
  unsigned int  state;
  const char	*csname;
  const char	*name;
  const char  *dir;
  unsigned int codepage;
  const char  *encoding;
  unsigned int	char_minlen;
  unsigned int	char_maxlen;
  unsigned int 	(*mb_charlen)(unsigned int c);
  unsigned int 	(*mb_valid)(const char *start, const char *end);
} MARIADB_CHARSET_INFO;

extern const MARIADB_CHARSET_INFO  mariadb_compiled_charsets[];
extern MARIADB_CHARSET_INFO *ma_default_charset_info;
extern MARIADB_CHARSET_INFO *ma_charset_bin;
extern MARIADB_CHARSET_INFO *ma_charset_latin1;
extern MARIADB_CHARSET_INFO *ma_charset_utf8_general_ci;
extern MARIADB_CHARSET_INFO *ma_charset_utf16le_general_ci;

MARIADB_CHARSET_INFO *find_compiled_charset(unsigned int cs_number);
MARIADB_CHARSET_INFO *find_compiled_charset_by_name(const char *name);

size_t mysql_cset_escape_quotes(const MARIADB_CHARSET_INFO *cset, char *newstr,  const char *escapestr, size_t escapestr_len);
size_t mysql_cset_escape_slashes(const MARIADB_CHARSET_INFO *cset, char *newstr, const char *escapestr, size_t escapestr_len);
const char* madb_get_os_character_set(void);
int madb_get_windows_cp(const char *charset);

typedef struct st_ma_const_string
{
  const char *str;
  size_t length;
} MARIADB_CONST_STRING;


  typedef struct st_ma_used_mem {   /* struct for once_alloc */
    struct st_ma_used_mem *next;    /* Next block in use */
    size_t left;                 /* memory left in block  */
    size_t size;                 /* Size of block */
  } MA_USED_MEM;

  typedef struct st_ma_mem_root {
    MA_USED_MEM *free;
    MA_USED_MEM *used;
    MA_USED_MEM *pre_alloc;
    size_t min_malloc;
    size_t block_size;
    unsigned int block_num;
    unsigned int first_block_usage;
    void (*error_handler)(void);
  } MA_MEM_ROOT;

extern unsigned int mysql_port;
extern char *mysql_unix_port;
extern unsigned int mariadb_deinitialize_ssl;

  typedef struct st_mysql_field {
    char *name;			/* Name of column */
    char *org_name;		/* Name of original column (added after 3.23.58) */
    char *table;			/* Table of column if column was a field */
    char *org_table;		/* Name of original table (added after 3.23.58 */
    char *db;                     /* table schema (added after 3.23.58) */
    char *catalog;                /* table catalog (added after 3.23.58) */
    char *def;			/* Default value (set by mysql_list_fields) */
    unsigned long length;		/* Width of column */
    unsigned long max_length;	/* Max width of selected set */
  /* added after 3.23.58 */
    unsigned int name_length;
    unsigned int org_name_length;
    unsigned int table_length;
    unsigned int org_table_length;
    unsigned int db_length;
    unsigned int catalog_length;
    unsigned int def_length;
  /***********************/
    unsigned int flags;		/* Div flags */
    unsigned int decimals;	/* Number of decimals in field */
    unsigned int charsetnr;       /* char set number (added in 4.1) */
    enum enum_field_types type;	/* Type of field. Se mysql_com.h for types */
    void *extension;              /* added in 4.1 */
  } MYSQL_FIELD;

  typedef char **MYSQL_ROW;		/* return data as array of strings */
  typedef unsigned int MYSQL_FIELD_OFFSET; /* offset to current field */

/* For mysql_async.c */
extern const char *SQLSTATE_UNKNOWN;

  typedef struct st_mysql_rows {
    struct st_mysql_rows *next;		/* list of rows */
    MYSQL_ROW data;
    unsigned long length;
  } MYSQL_ROWS;

  typedef MYSQL_ROWS *MYSQL_ROW_OFFSET;	/* offset to current row */

  typedef struct st_mysql_data {
    MYSQL_ROWS *data;
    void *embedded_info;
    MA_MEM_ROOT alloc;
    unsigned long long rows;
    unsigned int fields;
    void *extension;
  } MYSQL_DATA;

  enum mysql_option
  {
    MYSQL_OPT_CONNECT_TIMEOUT,
    MYSQL_OPT_COMPRESS,
    MYSQL_OPT_NAMED_PIPE,
    MYSQL_INIT_COMMAND,
    MYSQL_READ_DEFAULT_FILE,
    MYSQL_READ_DEFAULT_GROUP,
    MYSQL_SET_CHARSET_DIR,
    MYSQL_SET_CHARSET_NAME,
    MYSQL_OPT_LOCAL_INFILE,
    MYSQL_OPT_PROTOCOL,
    MYSQL_SHARED_MEMORY_BASE_NAME,
    MYSQL_OPT_READ_TIMEOUT,
    MYSQL_OPT_WRITE_TIMEOUT,
    MYSQL_OPT_USE_RESULT,
    MYSQL_OPT_USE_REMOTE_CONNECTION,
    MYSQL_OPT_USE_EMBEDDED_CONNECTION,
    MYSQL_OPT_GUESS_CONNECTION,
    MYSQL_SET_CLIENT_IP,
    MYSQL_SECURE_AUTH,
    MYSQL_REPORT_DATA_TRUNCATION,
    MYSQL_OPT_RECONNECT,
    MYSQL_OPT_SSL_VERIFY_SERVER_CERT,
    MYSQL_PLUGIN_DIR,
    MYSQL_DEFAULT_AUTH,
    MYSQL_OPT_BIND,
    MYSQL_OPT_SSL_KEY,
    MYSQL_OPT_SSL_CERT,
    MYSQL_OPT_SSL_CA,
    MYSQL_OPT_SSL_CAPATH,
    MYSQL_OPT_SSL_CIPHER,
    MYSQL_OPT_SSL_CRL,
    MYSQL_OPT_SSL_CRLPATH,
    /* Connection attribute options */
    MYSQL_OPT_CONNECT_ATTR_RESET,
    MYSQL_OPT_CONNECT_ATTR_ADD,
    MYSQL_OPT_CONNECT_ATTR_DELETE,
    MYSQL_SERVER_PUBLIC_KEY,
    MYSQL_ENABLE_CLEARTEXT_PLUGIN,
    MYSQL_OPT_CAN_HANDLE_EXPIRED_PASSWORDS,
    MYSQL_OPT_SSL_ENFORCE,
    MYSQL_OPT_MAX_ALLOWED_PACKET,
    MYSQL_OPT_NET_BUFFER_LENGTH,
    MYSQL_OPT_TLS_VERSION,

    /* MariaDB specific */
    MYSQL_PROGRESS_CALLBACK=5999,
    MYSQL_OPT_NONBLOCK,
    /* MariaDB Connector/C specific */
    MYSQL_DATABASE_DRIVER=7000,
    MARIADB_OPT_SSL_FP,             /* deprecated, use MARIADB_OPT_TLS_PEER_FP instead */
    MARIADB_OPT_SSL_FP_LIST,        /* deprecated, use MARIADB_OPT_TLS_PEER_FP_LIST instead */
    MARIADB_OPT_TLS_PASSPHRASE,     /* passphrase for encrypted certificates */
    MARIADB_OPT_TLS_CIPHER_STRENGTH,
    MARIADB_OPT_TLS_VERSION,
    MARIADB_OPT_TLS_PEER_FP,            /* single finger print for server certificate verification */
    MARIADB_OPT_TLS_PEER_FP_LIST,       /* finger print white list for server certificate verification */
    MARIADB_OPT_CONNECTION_READ_ONLY,
    MYSQL_OPT_CONNECT_ATTRS,        /* for mysql_get_optionv */
    MARIADB_OPT_USERDATA,
    MARIADB_OPT_CONNECTION_HANDLER,
    MARIADB_OPT_PORT,
    MARIADB_OPT_UNIXSOCKET,
    MARIADB_OPT_PASSWORD,
    MARIADB_OPT_HOST,
    MARIADB_OPT_USER,
    MARIADB_OPT_SCHEMA,
    MARIADB_OPT_DEBUG,
    MARIADB_OPT_FOUND_ROWS,
    MARIADB_OPT_MULTI_RESULTS,
    MARIADB_OPT_MULTI_STATEMENTS,
    MARIADB_OPT_INTERACTIVE,
    MARIADB_OPT_PROXY_HEADER,
    MARIADB_OPT_IO_WAIT,
    MARIADB_OPT_SKIP_READ_RESPONSE,
    MARIADB_OPT_RESTRICTED_AUTH,
    MARIADB_OPT_RPL_REGISTER_REPLICA,
    MARIADB_OPT_STATUS_CALLBACK
  };

  enum mariadb_value {
    MARIADB_CHARSET_ID,
    MARIADB_CHARSET_NAME,
    MARIADB_CLIENT_ERRORS,
    MARIADB_CLIENT_VERSION,
    MARIADB_CLIENT_VERSION_ID,
    MARIADB_CONNECTION_ASYNC_TIMEOUT,
    MARIADB_CONNECTION_ASYNC_TIMEOUT_MS,
    MARIADB_CONNECTION_MARIADB_CHARSET_INFO,
    MARIADB_CONNECTION_ERROR,
    MARIADB_CONNECTION_ERROR_ID,
    MARIADB_CONNECTION_HOST,
    MARIADB_CONNECTION_INFO,
    MARIADB_CONNECTION_PORT,
    MARIADB_CONNECTION_PROTOCOL_VERSION_ID,
    MARIADB_CONNECTION_PVIO_TYPE,
    MARIADB_CONNECTION_SCHEMA,
    MARIADB_CONNECTION_SERVER_TYPE,
    MARIADB_CONNECTION_SERVER_VERSION,
    MARIADB_CONNECTION_SERVER_VERSION_ID,
    MARIADB_CONNECTION_SOCKET,
    MARIADB_CONNECTION_SQLSTATE,
    MARIADB_CONNECTION_SSL_CIPHER,
    MARIADB_TLS_LIBRARY,
    MARIADB_CONNECTION_TLS_VERSION,
    MARIADB_CONNECTION_TLS_VERSION_ID,
    MARIADB_CONNECTION_TYPE,
    MARIADB_CONNECTION_UNIX_SOCKET,
    MARIADB_CONNECTION_USER,
    MARIADB_MAX_ALLOWED_PACKET,
    MARIADB_NET_BUFFER_LENGTH,
    MARIADB_CONNECTION_SERVER_STATUS,
    MARIADB_CONNECTION_SERVER_CAPABILITIES,
    MARIADB_CONNECTION_EXTENDED_SERVER_CAPABILITIES,
    MARIADB_CONNECTION_CLIENT_CAPABILITIES,
    MARIADB_CONNECTION_BYTES_READ,
    MARIADB_CONNECTION_BYTES_SENT
  };

  enum mysql_status { MYSQL_STATUS_READY,
                      MYSQL_STATUS_GET_RESULT,
                      MYSQL_STATUS_USE_RESULT,
                      MYSQL_STATUS_QUERY_SENT,
                      MYSQL_STATUS_SENDING_LOAD_DATA,
                      MYSQL_STATUS_FETCHING_DATA,
                      MYSQL_STATUS_NEXT_RESULT_PENDING,
                      MYSQL_STATUS_QUIT_SENT, /* object is "destroyed" at this stage */
                      MYSQL_STATUS_STMT_RESULT
  };

  enum mysql_protocol_type
  {
    MYSQL_PROTOCOL_DEFAULT, MYSQL_PROTOCOL_TCP, MYSQL_PROTOCOL_SOCKET,
    MYSQL_PROTOCOL_PIPE, MYSQL_PROTOCOL_MEMORY
  };

struct st_mysql_options {
    unsigned int connect_timeout, read_timeout, write_timeout;
    unsigned int port, protocol;
    unsigned long client_flag;
    char *host,*user,*password,*unix_socket,*db;
    struct st_dynamic_array *init_command;
    char *my_cnf_file,*my_cnf_group, *charset_dir, *charset_name;
    char *ssl_key;				/* PEM key file */
    char *ssl_cert;				/* PEM cert file */
    char *ssl_ca;					/* PEM CA file */
    char *ssl_capath;				/* PEM directory of CA-s? */
    char *ssl_cipher;
    char *shared_memory_base_name;
    unsigned long max_allowed_packet;
    my_bool use_ssl;				/* if to use SSL or not */
    my_bool compress,named_pipe;
    my_bool reconnect, unused_1, unused_2, unused_3;
    enum mysql_option methods_to_use;
    char *bind_address;
    my_bool secure_auth;
    my_bool report_data_truncation;
    /* function pointers for local infile support */
    int (*local_infile_init)(void **, const char *, void *);
    int (*local_infile_read)(void *, char *, unsigned int);
    void (*local_infile_end)(void *);
    int (*local_infile_error)(void *, char *, unsigned int);
    void *local_infile_userdata;
    struct st_mysql_options_extension *extension;
};

  typedef struct st_mysql {
    NET		net;			/* Communication parameters */
    void  *unused_0;
    char *host,*user,*passwd,*unix_socket,*server_version,*host_info;
    char *info,*db;
    const struct ma_charset_info_st *charset;      /* character set */
    MYSQL_FIELD *fields;
    MA_MEM_ROOT field_alloc;
    unsigned long long affected_rows;
    unsigned long long insert_id;		/* id if insert on table with NEXTNR */
    unsigned long long extra_info;		/* Used by mysqlshow */
    unsigned long thread_id;		/* Id for connection in server */
    unsigned long packet_length;
    unsigned int port;
    unsigned long client_flag;
    unsigned long server_capabilities;
    unsigned int protocol_version;
    unsigned int field_count;
    unsigned int server_status;
    unsigned int server_language;
    unsigned int warning_count;          /* warning count, added in 4.1 protocol */
    struct st_mysql_options options;
    enum mysql_status status;
    my_bool	free_me;		/* If free in mysql_close */
    my_bool	unused_1;
    char	        scramble_buff[20+ 1];
    /* madded after 3.23.58 */
    my_bool       unused_2;
    void          *unused_3, *unused_4, *unused_5, *unused_6;
    LIST          *stmts;
    const struct  st_mariadb_methods *methods;
    void          *thd;
    my_bool       *unbuffered_fetch_owner;
    char          *info_buffer;
    struct st_mariadb_extension *extension;
} MYSQL;

typedef struct st_mysql_res {
  unsigned long long  row_count;
  unsigned int	field_count, current_field;
  MYSQL_FIELD	*fields;
  MYSQL_DATA	*data;
  MYSQL_ROWS	*data_cursor;
  MA_MEM_ROOT	field_alloc;
  MYSQL_ROW	row;			/* If unbuffered read */
  MYSQL_ROW	current_row;		/* buffer to current row */
  unsigned long *lengths;		/* column lengths of current row */
  MYSQL		*handle;		/* for unbuffered reads */
  my_bool	eof;			/* Used my mysql_fetch_row */
  my_bool       is_ps;
} MYSQL_RES;

typedef struct
{
  unsigned long *p_max_allowed_packet;
  unsigned long *p_net_buffer_length;
  void *extension;
} MYSQL_PARAMETERS;


enum mariadb_field_attr_t
{
  MARIADB_FIELD_ATTR_DATA_TYPE_NAME= 0,
  MARIADB_FIELD_ATTR_FORMAT_NAME= 1
};


int mariadb_field_attr(MARIADB_CONST_STRING *attr,
                               const MYSQL_FIELD *field,
                               enum mariadb_field_attr_t type);

enum enum_mysql_timestamp_type
{
  MYSQL_TIMESTAMP_NONE= -2, MYSQL_TIMESTAMP_ERROR= -1,
  MYSQL_TIMESTAMP_DATE= 0, MYSQL_TIMESTAMP_DATETIME= 1, MYSQL_TIMESTAMP_TIME= 2
};

typedef struct st_mysql_time
{
  unsigned int  year, month, day, hour, minute, second;
  unsigned long second_part;
  my_bool       neg;
  enum enum_mysql_timestamp_type time_type;
} MYSQL_TIME;

typedef struct character_set
{
  unsigned int      number;     /* character set number              */
  unsigned int      state;      /* character set state               */
  const char        *csname;    /* character set name                */
  const char        *name;      /* collation name                    */
  const char        *comment;   /* comment                           */
  const char        *dir;       /* character set directory           */
  unsigned int      mbminlen;   /* min. length for multibyte strings */
  unsigned int      mbmaxlen;   /* max. length for multibyte strings */
} MY_CHARSET_INFO;


typedef struct st_mysql_stmt MYSQL_STMT;

typedef MYSQL_RES* (*mysql_stmt_use_or_store_func)(MYSQL_STMT *);

enum enum_stmt_attr_type
{
  STMT_ATTR_UPDATE_MAX_LENGTH,
  STMT_ATTR_CURSOR_TYPE,
  STMT_ATTR_PREFETCH_ROWS,

  /* MariaDB only */
  STMT_ATTR_PREBIND_PARAMS=200,
  STMT_ATTR_ARRAY_SIZE,
  STMT_ATTR_ROW_SIZE,
  STMT_ATTR_STATE,
  STMT_ATTR_CB_USER_DATA,
  STMT_ATTR_CB_PARAM,
  STMT_ATTR_CB_RESULT
};

enum enum_cursor_type
{
  CURSOR_TYPE_NO_CURSOR= 0,
  CURSOR_TYPE_READ_ONLY= 1,
  CURSOR_TYPE_FOR_UPDATE= 2,
  CURSOR_TYPE_SCROLLABLE= 4
};

enum enum_indicator_type
{
  STMT_INDICATOR_NTS=-1,
  STMT_INDICATOR_NONE=0,
  STMT_INDICATOR_NULL=1,
  STMT_INDICATOR_DEFAULT=2,
  STMT_INDICATOR_IGNORE=3,
  STMT_INDICATOR_IGNORE_ROW=4
};

typedef enum mysql_stmt_state
{
  MYSQL_STMT_INITTED = 0,
  MYSQL_STMT_PREPARED,
  MYSQL_STMT_EXECUTED,
  MYSQL_STMT_WAITING_USE_OR_STORE,
  MYSQL_STMT_USE_OR_STORE_CALLED,
  MYSQL_STMT_USER_FETCHING, /* fetch_row_buff or fetch_row_unbuf */
  MYSQL_STMT_FETCH_DONE
} enum_mysqlnd_stmt_state;

typedef struct st_mysql_bind
{
  unsigned long  *length;          /* output length pointer */
  my_bool        *is_null;         /* Pointer to null indicator */
  void           *buffer;          /* buffer to get/put data */
  /* set this if you want to track data truncations happened during fetch */
  my_bool        *error;
  union {
    unsigned char *row_ptr;        /* for the current data position */
    char *indicator;               /* indicator variable */
  } u;
  void (*store_param_func)(NET *net, struct st_mysql_bind *param);
  void (*fetch_result)(struct st_mysql_bind *, MYSQL_FIELD *,
                       unsigned char **row);
  void (*skip_result)(struct st_mysql_bind *, MYSQL_FIELD *,
          unsigned char **row);
  /* output buffer length, must be set when fetching str/binary */
  unsigned long  buffer_length;
  unsigned long  offset;           /* offset position for char/binary fetch */
  unsigned long  length_value;     /* Used if length is 0 */
  unsigned int   flags;            /* special flags, e.g. for dummy bind  */
  unsigned int   pack_length;      /* Internal length for packed data */
  enum enum_field_types buffer_type;  /* buffer type */
  my_bool        error_value;      /* used if error is 0 */
  my_bool        is_unsigned;      /* set if integer type is unsigned */
  my_bool        long_data_used;   /* If used with mysql_send_long_data */
  my_bool        is_null_value;    /* Used if is_null is 0 */
  void           *extension;
} MYSQL_BIND;

typedef struct st_mysqlnd_upsert_result
{
  unsigned int  warning_count;
  unsigned int  server_status;
  unsigned long long affected_rows;
  unsigned long long last_insert_id;
} mysql_upsert_status;

typedef struct st_mysql_cmd_buffer
{
  unsigned char   *buffer;
  size_t     length;
} MYSQL_CMD_BUFFER;

typedef struct st_mysql_error_info
{
  unsigned int error_no;
  char error[MYSQL_ERRMSG_SIZE+1];
  char sqlstate[SQLSTATE_LENGTH + 1];
} mysql_error_info;

typedef int  (*mysql_stmt_fetch_row_func)(MYSQL_STMT *stmt, unsigned char **row);
typedef void (*ps_result_callback)(void *data, unsigned int column, unsigned char **row);
typedef my_bool *(*ps_param_callback)(void *data, MYSQL_BIND *bind, unsigned int row_nr);

struct st_mysql_stmt
{
  MA_MEM_ROOT              mem_root;
  MYSQL                    *mysql;
  unsigned long            stmt_id;
  unsigned long            flags;/* cursor is set here */
  enum_mysqlnd_stmt_state  state;
  MYSQL_FIELD              *fields;
  unsigned int             field_count;
  unsigned int             param_count;
  unsigned char            send_types_to_server;
  MYSQL_BIND               *params;
  MYSQL_BIND               *bind;
  MYSQL_DATA               result;  /* we don't use mysqlnd's result set logic */
  MYSQL_ROWS               *result_cursor;
  my_bool                  bind_result_done;
  my_bool                  bind_param_done;

  mysql_upsert_status      upsert_status;

  unsigned int last_errno;
  char last_error[MYSQL_ERRMSG_SIZE+1];
  char sqlstate[SQLSTATE_LENGTH + 1];

  my_bool                  update_max_length;
  unsigned long            prefetch_rows;
  LIST                     list;

  my_bool                  cursor_exists;

  void                     *extension;
  mysql_stmt_fetch_row_func fetch_row_func;
  unsigned int             execute_count;/* count how many times the stmt was executed */
  mysql_stmt_use_or_store_func default_rset_handler;
  unsigned char            *request_buffer;
  unsigned int             array_size;
  size_t row_size;
  unsigned int prebind_params;
  void *user_data;
  ps_result_callback result_callback;
  ps_param_callback param_callback;
  size_t request_length;
};

typedef void (*ps_field_fetch_func)(MYSQL_BIND *r_param, const MYSQL_FIELD * field, unsigned char **row);
typedef struct st_mysql_perm_bind {
  ps_field_fetch_func func;
  /* should be signed int */
  int pack_len;
  unsigned long max_len;
} MYSQL_PS_CONVERSION;

extern MYSQL_PS_CONVERSION mysql_ps_fetch_functions[MYSQL_TYPE_GEOMETRY + 1];
unsigned long ma_net_safe_read(MYSQL *mysql);
void mysql_init_ps_subsystem(void);
unsigned long net_field_length(unsigned char **packet);
int ma_simple_command(MYSQL *mysql,enum enum_server_command command, const char *arg,
          	       size_t length, my_bool skipp_check, void *opt_arg);
void stmt_set_error(MYSQL_STMT *stmt,
                  unsigned int error_nr,
                  const char *sqlstate,
                  const char *format,
                  ...);
/*
 *  function prototypes
 */
MYSQL_STMT * mysql_stmt_init(MYSQL *mysql);
int mysql_stmt_prepare(MYSQL_STMT *stmt, const char *query, unsigned long length);
int mysql_stmt_execute(MYSQL_STMT *stmt);
int mysql_stmt_fetch(MYSQL_STMT *stmt);
int mysql_stmt_fetch_column(MYSQL_STMT *stmt, MYSQL_BIND *bind_arg, unsigned int column, unsigned long offset);
int mysql_stmt_store_result(MYSQL_STMT *stmt);
unsigned long mysql_stmt_param_count(MYSQL_STMT * stmt);
my_bool mysql_stmt_attr_set(MYSQL_STMT *stmt, enum enum_stmt_attr_type attr_type, const void *attr);
my_bool mysql_stmt_attr_get(MYSQL_STMT *stmt, enum enum_stmt_attr_type attr_type, void *attr);
my_bool mysql_stmt_bind_param(MYSQL_STMT * stmt, MYSQL_BIND * bnd);
my_bool mysql_stmt_bind_result(MYSQL_STMT * stmt, MYSQL_BIND * bnd);
my_bool mysql_stmt_close(MYSQL_STMT * stmt);
my_bool mysql_stmt_reset(MYSQL_STMT * stmt);
my_bool mysql_stmt_free_result(MYSQL_STMT *stmt);
my_bool mysql_stmt_send_long_data(MYSQL_STMT *stmt, unsigned int param_number, const char *data, unsigned long length);
MYSQL_RES *mysql_stmt_result_metadata(MYSQL_STMT *stmt);
MYSQL_RES *mysql_stmt_param_metadata(MYSQL_STMT *stmt);
unsigned int mysql_stmt_errno(MYSQL_STMT * stmt);
const char *mysql_stmt_error(MYSQL_STMT * stmt);
const char *mysql_stmt_sqlstate(MYSQL_STMT * stmt);
MYSQL_ROW_OFFSET mysql_stmt_row_seek(MYSQL_STMT *stmt, MYSQL_ROW_OFFSET offset);
MYSQL_ROW_OFFSET mysql_stmt_row_tell(MYSQL_STMT *stmt);
void mysql_stmt_data_seek(MYSQL_STMT *stmt, unsigned long long offset);
unsigned long long mysql_stmt_num_rows(MYSQL_STMT *stmt);
unsigned long long mysql_stmt_affected_rows(MYSQL_STMT *stmt);
unsigned long long mysql_stmt_insert_id(MYSQL_STMT *stmt);
unsigned int mysql_stmt_field_count(MYSQL_STMT *stmt);
int mysql_stmt_next_result(MYSQL_STMT *stmt);
my_bool mysql_stmt_more_results(MYSQL_STMT *stmt);
int mariadb_stmt_execute_direct(MYSQL_STMT *stmt, const char *stmt_str, size_t length);
MYSQL_FIELD * mariadb_stmt_fetch_fields(MYSQL_STMT *stmt);

struct st_mysql_client_plugin
{
  int type;
  unsigned int interface_version;
  const char *name;
  const char *author;
  const char *desc;
  unsigned int version[3];
  const char *license;
  void *mysql_api;
  int (*init)(char *, size_t, int, va_list);
  int (*deinit)(void);
  int (*options)(const char *option, const void *);
};

struct st_mysql_client_plugin *
mysql_load_plugin(struct st_mysql *mysql, const char *name, int type,
                  int argc, ...);
struct st_mysql_client_plugin *
mysql_load_plugin_v(struct st_mysql *mysql, const char *name, int type,
                    int argc, va_list args);
struct st_mysql_client_plugin *
mysql_client_find_plugin(struct st_mysql *mysql, const char *name, int type);
struct st_mysql_client_plugin *
mysql_client_register_plugin(struct st_mysql *mysql,
                             struct st_mysql_client_plugin *plugin);

void mysql_set_local_infile_handler(MYSQL *mysql,
        int (*local_infile_init)(void **, const char *, void *),
        int (*local_infile_read)(void *, char *, unsigned int),
        void (*local_infile_end)(void *),
        int (*local_infile_error)(void *, char*, unsigned int),
        void *);

void mysql_set_local_infile_default(MYSQL *mysql);

void my_set_error(MYSQL *mysql, unsigned int error_nr,
                  const char *sqlstate, const char *format, ...);
/* Functions to get information from the MYSQL and MYSQL_RES structures */
/* Should definitely be used if one uses shared libraries */

my_ulonglong mysql_num_rows(MYSQL_RES *res);
unsigned int mysql_num_fields(MYSQL_RES *res);
my_bool mysql_eof(MYSQL_RES *res);
MYSQL_FIELD *mysql_fetch_field_direct(MYSQL_RES *res,
					      unsigned int fieldnr);
MYSQL_FIELD * mysql_fetch_fields(MYSQL_RES *res);
MYSQL_ROWS * mysql_row_tell(MYSQL_RES *res);
unsigned int mysql_field_tell(MYSQL_RES *res);

unsigned int mysql_field_count(MYSQL *mysql);
my_bool mysql_more_results(MYSQL *mysql);
int mysql_next_result(MYSQL *mysql);
my_ulonglong mysql_affected_rows(MYSQL *mysql);
my_bool mysql_autocommit(MYSQL *mysql, my_bool mode);
my_bool mysql_commit(MYSQL *mysql);
my_bool mysql_rollback(MYSQL *mysql);
my_ulonglong mysql_insert_id(MYSQL *mysql);
unsigned int mysql_errno(MYSQL *mysql);
const char * mysql_error(MYSQL *mysql);
const char * mysql_info(MYSQL *mysql);
unsigned long mysql_thread_id(MYSQL *mysql);
const char * mysql_character_set_name(MYSQL *mysql);
void mysql_get_character_set_info(MYSQL *mysql, MY_CHARSET_INFO *cs);
int mysql_set_character_set(MYSQL *mysql, const char *csname);

my_bool mariadb_get_infov(MYSQL *mysql, enum mariadb_value value, void *arg, ...);
my_bool mariadb_get_info(MYSQL *mysql, enum mariadb_value value, void *arg);
MYSQL *		mysql_init(MYSQL *mysql);
int		mysql_ssl_set(MYSQL *mysql, const char *key,
				      const char *cert, const char *ca,
				      const char *capath, const char *cipher);
const char *	mysql_get_ssl_cipher(MYSQL *mysql);
my_bool		mysql_change_user(MYSQL *mysql, const char *user,
					  const char *passwd, const char *db);
MYSQL *		mysql_real_connect(MYSQL *mysql, const char *host,
					   const char *user,
					   const char *passwd,
					   const char *db,
					   unsigned int port,
					   const char *unix_socket,
					   unsigned long clientflag);
void		mysql_close(MYSQL *sock);
int		mysql_select_db(MYSQL *mysql, const char *db);
int		mysql_query(MYSQL *mysql, const char *q);
int		mysql_send_query(MYSQL *mysql, const char *q,
					 unsigned long length);
my_bool	mysql_read_query_result(MYSQL *mysql);
int		mysql_real_query(MYSQL *mysql, const char *q,
					 unsigned long length);
int		mysql_shutdown(MYSQL *mysql, enum mysql_enum_shutdown_level shutdown_level);
int		mysql_dump_debug_info(MYSQL *mysql);
int		mysql_refresh(MYSQL *mysql,
				     unsigned int refresh_options);
int		mysql_kill(MYSQL *mysql,unsigned long pid);
int		mysql_ping(MYSQL *mysql);
char *		mysql_stat(MYSQL *mysql);
char *		mysql_get_server_info(MYSQL *mysql);
unsigned long   mysql_get_server_version(MYSQL *mysql);
char *		mysql_get_host_info(MYSQL *mysql);
unsigned int	mysql_get_proto_info(MYSQL *mysql);
MYSQL_RES *	mysql_list_dbs(MYSQL *mysql,const char *wild);
MYSQL_RES *	mysql_list_tables(MYSQL *mysql,const char *wild);
MYSQL_RES *	mysql_list_fields(MYSQL *mysql, const char *table,
					 const char *wild);
MYSQL_RES *	mysql_list_processes(MYSQL *mysql);
MYSQL_RES *	mysql_store_result(MYSQL *mysql);
MYSQL_RES *	mysql_use_result(MYSQL *mysql);
int		mysql_options(MYSQL *mysql,enum mysql_option option,
				      const void *arg);
int		mysql_options4(MYSQL *mysql,enum mysql_option option,
				      const void *arg1, const void *arg2);
void		mysql_free_result(MYSQL_RES *result);
void		mysql_data_seek(MYSQL_RES *result,
					unsigned long long offset);
MYSQL_ROW_OFFSET mysql_row_seek(MYSQL_RES *result, MYSQL_ROW_OFFSET);
MYSQL_FIELD_OFFSET mysql_field_seek(MYSQL_RES *result,
					   MYSQL_FIELD_OFFSET offset);
MYSQL_ROW	mysql_fetch_row(MYSQL_RES *result);
unsigned long * mysql_fetch_lengths(MYSQL_RES *result);
MYSQL_FIELD *	mysql_fetch_field(MYSQL_RES *result);
unsigned long	mysql_escape_string(char *to,const char *from,
					    unsigned long from_length);
unsigned long mysql_real_escape_string(MYSQL *mysql,
					       char *to,const char *from,
					       unsigned long length);
unsigned int	mysql_thread_safe(void);
unsigned int mysql_warning_count(MYSQL *mysql);
const char * mysql_sqlstate(MYSQL *mysql);
int mysql_server_init(int argc, char **argv, char **groups);
void mysql_server_end(void);
void mysql_thread_end(void);
my_bool mysql_thread_init(void);
int mysql_set_server_option(MYSQL *mysql,
                                    enum enum_mysql_set_option option);
const char * mysql_get_client_info(void);
unsigned long mysql_get_client_version(void);
my_bool mariadb_connection(MYSQL *mysql);
const char * mysql_get_server_name(MYSQL *mysql);
MARIADB_CHARSET_INFO * mariadb_get_charset_by_name(const char *csname);
MARIADB_CHARSET_INFO * mariadb_get_charset_by_nr(unsigned int csnr);
size_t mariadb_convert_string(const char *from, size_t *from_len, MARIADB_CHARSET_INFO *from_cs,
                                      char *to, size_t *to_len, MARIADB_CHARSET_INFO *to_cs, int *errorcode);
int mysql_optionsv(MYSQL *mysql,enum mysql_option option, ...);
int mysql_get_optionv(MYSQL *mysql, enum mysql_option option, void *arg, ...);
int mysql_get_option(MYSQL *mysql, enum mysql_option option, void *arg);
unsigned long mysql_hex_string(char *to, const char *from, unsigned long len);
my_socket mysql_get_socket(MYSQL *mysql);
unsigned int mysql_get_timeout_value(const MYSQL *mysql);
unsigned int mysql_get_timeout_value_ms(const MYSQL *mysql);
my_bool mariadb_reconnect(MYSQL *mysql);
int mariadb_cancel(MYSQL *mysql);
void mysql_debug(const char *debug);
unsigned long mysql_net_read_packet(MYSQL *mysql);
unsigned long mysql_net_field_length(unsigned char **packet);
my_bool mysql_embedded(void);
MYSQL_PARAMETERS *mysql_get_parameters(void);

/* Async API */
int mysql_close_start(MYSQL *sock);
int mysql_close_cont(MYSQL *sock, int status);
int mysql_commit_start(my_bool *ret, MYSQL * mysql);
int mysql_commit_cont(my_bool *ret, MYSQL * mysql, int status);
int mysql_dump_debug_info_cont(int *ret, MYSQL *mysql, int ready_status);
int mysql_dump_debug_info_start(int *ret, MYSQL *mysql);
int mysql_rollback_start(my_bool *ret, MYSQL * mysql);
int mysql_rollback_cont(my_bool *ret, MYSQL * mysql, int status);
int mysql_autocommit_start(my_bool *ret, MYSQL * mysql,
                                   my_bool auto_mode);
int mysql_list_fields_cont(MYSQL_RES **ret, MYSQL *mysql, int ready_status);
int mysql_list_fields_start(MYSQL_RES **ret, MYSQL *mysql, const char *table,
                        const char *wild);
int mysql_autocommit_cont(my_bool *ret, MYSQL * mysql, int status);
int mysql_next_result_start(int *ret, MYSQL *mysql);
int mysql_next_result_cont(int *ret, MYSQL *mysql, int status);
int mysql_select_db_start(int *ret, MYSQL *mysql, const char *db);
int mysql_select_db_cont(int *ret, MYSQL *mysql, int ready_status);
int mysql_stmt_warning_count(MYSQL_STMT *stmt);
int mysql_stmt_next_result_start(int *ret, MYSQL_STMT *stmt);
int mysql_stmt_next_result_cont(int *ret, MYSQL_STMT *stmt, int status);

int mysql_set_character_set_start(int *ret, MYSQL *mysql,
                                                   const char *csname);
int mysql_set_character_set_cont(int *ret, MYSQL *mysql,
                                                  int status);
int mysql_change_user_start(my_bool *ret, MYSQL *mysql,
                                                const char *user,
                                                const char *passwd,
                                                const char *db);
int mysql_change_user_cont(my_bool *ret, MYSQL *mysql,
                                               int status);
int         mysql_real_connect_start(MYSQL **ret, MYSQL *mysql,
                                                 const char *host,
                                                 const char *user,
                                                 const char *passwd,
                                                 const char *db,
                                                 unsigned int port,
                                                 const char *unix_socket,
                                                 unsigned long clientflag);
int         mysql_real_connect_cont(MYSQL **ret, MYSQL *mysql,
                                                int status);
int             mysql_query_start(int *ret, MYSQL *mysql,
                                          const char *q);
int             mysql_query_cont(int *ret, MYSQL *mysql,
                                         int status);
int             mysql_send_query_start(int *ret, MYSQL *mysql,
                                               const char *q,
                                               unsigned long length);
int             mysql_send_query_cont(int *ret, MYSQL *mysql, int status);
int             mysql_real_query_start(int *ret, MYSQL *mysql,
                                               const char *q,
                                               unsigned long length);
int             mysql_real_query_cont(int *ret, MYSQL *mysql,
                                              int status);
int             mysql_store_result_start(MYSQL_RES **ret, MYSQL *mysql);
int             mysql_store_result_cont(MYSQL_RES **ret, MYSQL *mysql,
                                                int status);
int             mysql_shutdown_start(int *ret, MYSQL *mysql,
                                             enum mysql_enum_shutdown_level
                                             shutdown_level);
int             mysql_shutdown_cont(int *ret, MYSQL *mysql,
                                            int status);
int             mysql_refresh_start(int *ret, MYSQL *mysql,
                                            unsigned int refresh_options);
int             mysql_refresh_cont(int *ret, MYSQL *mysql, int status);
int             mysql_kill_start(int *ret, MYSQL *mysql,
                                         unsigned long pid);
int             mysql_kill_cont(int *ret, MYSQL *mysql, int status);
int             mysql_set_server_option_start(int *ret, MYSQL *mysql,
                                                      enum enum_mysql_set_option
                                                      option);
int             mysql_set_server_option_cont(int *ret, MYSQL *mysql,
                                                     int status);
int             mysql_ping_start(int *ret, MYSQL *mysql);
int             mysql_ping_cont(int *ret, MYSQL *mysql, int status);
int             mysql_stat_start(const char **ret, MYSQL *mysql);
int             mysql_stat_cont(const char **ret, MYSQL *mysql,
                                        int status);
int             mysql_free_result_start(MYSQL_RES *result);
int             mysql_free_result_cont(MYSQL_RES *result, int status);
int             mysql_fetch_row_start(MYSQL_ROW *ret,
                                              MYSQL_RES *result);
int             mysql_fetch_row_cont(MYSQL_ROW *ret, MYSQL_RES *result,
                                             int status);
int             mysql_read_query_result_start(my_bool *ret,
                                                      MYSQL *mysql);
int             mysql_read_query_result_cont(my_bool *ret,
                                                     MYSQL *mysql, int status);
int             mysql_reset_connection_start(int *ret, MYSQL *mysql);
int             mysql_reset_connection_cont(int *ret, MYSQL *mysql, int status);
int mysql_session_track_get_next(MYSQL *mysql, enum enum_session_state_type type, const char **data, size_t *length);
int mysql_session_track_get_first(MYSQL *mysql, enum enum_session_state_type type, const char **data, size_t *length);
int mysql_stmt_prepare_start(int *ret, MYSQL_STMT *stmt,const char *query, unsigned long length);
int mysql_stmt_prepare_cont(int *ret, MYSQL_STMT *stmt, int status);
int mysql_stmt_execute_start(int *ret, MYSQL_STMT *stmt);
int mysql_stmt_execute_cont(int *ret, MYSQL_STMT *stmt, int status);
int mysql_stmt_fetch_start(int *ret, MYSQL_STMT *stmt);
int mysql_stmt_fetch_cont(int *ret, MYSQL_STMT *stmt, int status);
int mysql_stmt_store_result_start(int *ret, MYSQL_STMT *stmt);
int mysql_stmt_store_result_cont(int *ret, MYSQL_STMT *stmt,int status);
int mysql_stmt_close_start(my_bool *ret, MYSQL_STMT *stmt);
int mysql_stmt_close_cont(my_bool *ret, MYSQL_STMT * stmt, int status);
int mysql_stmt_reset_start(my_bool *ret, MYSQL_STMT * stmt);
int mysql_stmt_reset_cont(my_bool *ret, MYSQL_STMT *stmt, int status);
int mysql_stmt_free_result_start(my_bool *ret, MYSQL_STMT *stmt);
int mysql_stmt_free_result_cont(my_bool *ret, MYSQL_STMT *stmt,
                                        int status);
int mysql_stmt_send_long_data_start(my_bool *ret, MYSQL_STMT *stmt,
                                            unsigned int param_number,
                                            const char *data,
                                            unsigned long len);
int mysql_stmt_send_long_data_cont(my_bool *ret, MYSQL_STMT *stmt,
                                           int status);
int mysql_reset_connection(MYSQL *mysql);

/* API function calls (used by dynamic plugins) */
struct st_mariadb_api {
  unsigned long long (*mysql_num_rows)(MYSQL_RES *res);
  unsigned int (*mysql_num_fields)(MYSQL_RES *res);
  my_bool (*mysql_eof)(MYSQL_RES *res);
  MYSQL_FIELD *(*mysql_fetch_field_direct)(MYSQL_RES *res, unsigned int fieldnr);
  MYSQL_FIELD * (*mysql_fetch_fields)(MYSQL_RES *res);
  MYSQL_ROWS * (*mysql_row_tell)(MYSQL_RES *res);
  unsigned int (*mysql_field_tell)(MYSQL_RES *res);
  unsigned int (*mysql_field_count)(MYSQL *mysql);
  my_bool (*mysql_more_results)(MYSQL *mysql);
  int (*mysql_next_result)(MYSQL *mysql);
  unsigned long long (*mysql_affected_rows)(MYSQL *mysql);
  my_bool (*mysql_autocommit)(MYSQL *mysql, my_bool mode);
  my_bool (*mysql_commit)(MYSQL *mysql);
  my_bool (*mysql_rollback)(MYSQL *mysql);
  unsigned long long (*mysql_insert_id)(MYSQL *mysql);
  unsigned int (*mysql_errno)(MYSQL *mysql);
  const char * (*mysql_error)(MYSQL *mysql);
  const char * (*mysql_info)(MYSQL *mysql);
  unsigned long (*mysql_thread_id)(MYSQL *mysql);
  const char * (*mysql_character_set_name)(MYSQL *mysql);
  void (*mysql_get_character_set_info)(MYSQL *mysql, MY_CHARSET_INFO *cs);
  int (*mysql_set_character_set)(MYSQL *mysql, const char *csname);
  my_bool (*mariadb_get_infov)(MYSQL *mysql, enum mariadb_value value, void *arg, ...);
  my_bool (*mariadb_get_info)(MYSQL *mysql, enum mariadb_value value, void *arg);
  MYSQL * (*mysql_init)(MYSQL *mysql);
  int (*mysql_ssl_set)(MYSQL *mysql, const char *key, const char *cert, const char *ca, const char *capath, const char *cipher);
  const char * (*mysql_get_ssl_cipher)(MYSQL *mysql);
  my_bool (*mysql_change_user)(MYSQL *mysql, const char *user, const char *passwd, const char *db);
  MYSQL * (*mysql_real_connect)(MYSQL *mysql, const char *host, const char *user, const char *passwd, const char *db, unsigned int port, const char *unix_socket, unsigned long clientflag);
  void (*mysql_close)(MYSQL *sock);
  int (*mysql_select_db)(MYSQL *mysql, const char *db);
  int (*mysql_query)(MYSQL *mysql, const char *q);
  int (*mysql_send_query)(MYSQL *mysql, const char *q, unsigned long length);
  my_bool (*mysql_read_query_result)(MYSQL *mysql);
  int (*mysql_real_query)(MYSQL *mysql, const char *q, unsigned long length);
  int (*mysql_shutdown)(MYSQL *mysql, enum mysql_enum_shutdown_level shutdown_level);
  int (*mysql_dump_debug_info)(MYSQL *mysql);
  int (*mysql_refresh)(MYSQL *mysql, unsigned int refresh_options);
  int (*mysql_kill)(MYSQL *mysql,unsigned long pid);
  int (*mysql_ping)(MYSQL *mysql);
  char * (*mysql_stat)(MYSQL *mysql);
  char * (*mysql_get_server_info)(MYSQL *mysql);
  unsigned long (*mysql_get_server_version)(MYSQL *mysql);
  char * (*mysql_get_host_info)(MYSQL *mysql);
  unsigned int (*mysql_get_proto_info)(MYSQL *mysql);
  MYSQL_RES * (*mysql_list_dbs)(MYSQL *mysql,const char *wild);
  MYSQL_RES * (*mysql_list_tables)(MYSQL *mysql,const char *wild);
  MYSQL_RES * (*mysql_list_fields)(MYSQL *mysql, const char *table, const char *wild);
  MYSQL_RES * (*mysql_list_processes)(MYSQL *mysql);
  MYSQL_RES * (*mysql_store_result)(MYSQL *mysql);
  MYSQL_RES * (*mysql_use_result)(MYSQL *mysql);
  int (*mysql_options)(MYSQL *mysql,enum mysql_option option, const void *arg);
  void (*mysql_free_result)(MYSQL_RES *result);
  void (*mysql_data_seek)(MYSQL_RES *result, unsigned long long offset);
  MYSQL_ROW_OFFSET (*mysql_row_seek)(MYSQL_RES *result, MYSQL_ROW_OFFSET);
  MYSQL_FIELD_OFFSET (*mysql_field_seek)(MYSQL_RES *result, MYSQL_FIELD_OFFSET offset);
  MYSQL_ROW (*mysql_fetch_row)(MYSQL_RES *result);
  unsigned long * (*mysql_fetch_lengths)(MYSQL_RES *result);
  MYSQL_FIELD * (*mysql_fetch_field)(MYSQL_RES *result);
  unsigned long (*mysql_escape_string)(char *to,const char *from, unsigned long from_length);
  unsigned long (*mysql_real_escape_string)(MYSQL *mysql, char *to,const char *from, unsigned long length);
  unsigned int (*mysql_thread_safe)(void);
  unsigned int (*mysql_warning_count)(MYSQL *mysql);
  const char * (*mysql_sqlstate)(MYSQL *mysql);
  int (*mysql_server_init)(int argc, char **argv, char **groups);
  void (*mysql_server_end)(void);
  void (*mysql_thread_end)(void);
  my_bool (*mysql_thread_init)(void);
  int (*mysql_set_server_option)(MYSQL *mysql, enum enum_mysql_set_option option);
  const char * (*mysql_get_client_info)(void);
  unsigned long (*mysql_get_client_version)(void);
  my_bool (*mariadb_connection)(MYSQL *mysql);
  const char * (*mysql_get_server_name)(MYSQL *mysql);
  MARIADB_CHARSET_INFO * (*mariadb_get_charset_by_name)(const char *csname);
  MARIADB_CHARSET_INFO * (*mariadb_get_charset_by_nr)(unsigned int csnr);
  size_t (*mariadb_convert_string)(const char *from, size_t *from_len, MARIADB_CHARSET_INFO *from_cs, char *to, size_t *to_len, MARIADB_CHARSET_INFO *to_cs, int *errorcode);
  int (*mysql_optionsv)(MYSQL *mysql,enum mysql_option option, ...);
  int (*mysql_get_optionv)(MYSQL *mysql, enum mysql_option option, void *arg, ...);
  int (*mysql_get_option)(MYSQL *mysql, enum mysql_option option, void *arg);
  unsigned long (*mysql_hex_string)(char *to, const char *from, unsigned long len);
  my_socket (*mysql_get_socket)(MYSQL *mysql);
  unsigned int (*mysql_get_timeout_value)(const MYSQL *mysql);
  unsigned int (*mysql_get_timeout_value_ms)(const MYSQL *mysql);
  my_bool (*mariadb_reconnect)(MYSQL *mysql);
  MYSQL_STMT * (*mysql_stmt_init)(MYSQL *mysql);
  int (*mysql_stmt_prepare)(MYSQL_STMT *stmt, const char *query, unsigned long length);
  int (*mysql_stmt_execute)(MYSQL_STMT *stmt);
  int (*mysql_stmt_fetch)(MYSQL_STMT *stmt);
  int (*mysql_stmt_fetch_column)(MYSQL_STMT *stmt, MYSQL_BIND *bind_arg, unsigned int column, unsigned long offset);
  int (*mysql_stmt_store_result)(MYSQL_STMT *stmt);
  unsigned long (*mysql_stmt_param_count)(MYSQL_STMT * stmt);
  my_bool (*mysql_stmt_attr_set)(MYSQL_STMT *stmt, enum enum_stmt_attr_type attr_type, const void *attr);
  my_bool (*mysql_stmt_attr_get)(MYSQL_STMT *stmt, enum enum_stmt_attr_type attr_type, void *attr);
  my_bool (*mysql_stmt_bind_param)(MYSQL_STMT * stmt, MYSQL_BIND * bnd);
  my_bool (*mysql_stmt_bind_result)(MYSQL_STMT * stmt, MYSQL_BIND * bnd);
  my_bool (*mysql_stmt_close)(MYSQL_STMT * stmt);
  my_bool (*mysql_stmt_reset)(MYSQL_STMT * stmt);
  my_bool (*mysql_stmt_free_result)(MYSQL_STMT *stmt);
  my_bool (*mysql_stmt_send_long_data)(MYSQL_STMT *stmt, unsigned int param_number, const char *data, unsigned long length);
  MYSQL_RES *(*mysql_stmt_result_metadata)(MYSQL_STMT *stmt);
  MYSQL_RES *(*mysql_stmt_param_metadata)(MYSQL_STMT *stmt);
  unsigned int (*mysql_stmt_errno)(MYSQL_STMT * stmt);
  const char *(*mysql_stmt_error)(MYSQL_STMT * stmt);
  const char *(*mysql_stmt_sqlstate)(MYSQL_STMT * stmt);
  MYSQL_ROW_OFFSET (*mysql_stmt_row_seek)(MYSQL_STMT *stmt, MYSQL_ROW_OFFSET offset);
  MYSQL_ROW_OFFSET (*mysql_stmt_row_tell)(MYSQL_STMT *stmt);
  void (*mysql_stmt_data_seek)(MYSQL_STMT *stmt, unsigned long long offset);
  unsigned long long (*mysql_stmt_num_rows)(MYSQL_STMT *stmt);
  unsigned long long (*mysql_stmt_affected_rows)(MYSQL_STMT *stmt);
  unsigned long long (*mysql_stmt_insert_id)(MYSQL_STMT *stmt);
  unsigned int (*mysql_stmt_field_count)(MYSQL_STMT *stmt);
  int (*mysql_stmt_next_result)(MYSQL_STMT *stmt);
  my_bool (*mysql_stmt_more_results)(MYSQL_STMT *stmt);
  int (*mariadb_stmt_execute_direct)(MYSQL_STMT *stmt, const char *stmtstr, size_t length);
  int (*mysql_reset_connection)(MYSQL *mysql);
};

/* these methods can be overwritten by db plugins */
struct st_mariadb_methods {
  MYSQL *(*db_connect)(MYSQL *mysql, const char *host, const char *user, const char *passwd,
					   const char *db, unsigned int port, const char *unix_socket, unsigned long clientflag);
  void (*db_close)(MYSQL *mysql);
  int (*db_command)(MYSQL *mysql,enum enum_server_command command, const char *arg,
                    size_t length, my_bool skip_check, void *opt_arg);
  void (*db_skip_result)(MYSQL *mysql);
  int (*db_read_query_result)(MYSQL *mysql);
  MYSQL_DATA *(*db_read_rows)(MYSQL *mysql,MYSQL_FIELD *fields, unsigned int field_count);
  int (*db_read_one_row)(MYSQL *mysql,unsigned int fields,MYSQL_ROW row, unsigned long *lengths);
  /* prepared statements */
  my_bool (*db_supported_buffer_type)(enum enum_field_types type);
  my_bool (*db_read_prepare_response)(MYSQL_STMT *stmt);
  int (*db_read_stmt_result)(MYSQL *mysql);
  my_bool (*db_stmt_get_result_metadata)(MYSQL_STMT *stmt);
  my_bool (*db_stmt_get_param_metadata)(MYSQL_STMT *stmt);
  int (*db_stmt_read_all_rows)(MYSQL_STMT *stmt);
  int (*db_stmt_fetch)(MYSQL_STMT *stmt, unsigned char **row);
  int (*db_stmt_fetch_to_bind)(MYSQL_STMT *stmt, unsigned char *row);
  void (*db_stmt_flush_unbuffered)(MYSQL_STMT *stmt);
  void (*set_error)(MYSQL *mysql, unsigned int error_nr, const char *sqlstate, const char *format, ...);
  void (*invalidate_stmts)(MYSQL *mysql, const char *function_name);
  struct st_mariadb_api *api;
  int (*db_read_execute_response)(MYSQL_STMT *stmt);
  unsigned char* (*db_execute_generate_request)(MYSQL_STMT *stmt, size_t *request_len, my_bool internal);
};
