# MariaDB Connector/LuaJIT

### What is MariaDB Connector/LuaJIT?
As the name suggests, MariaDB Connector/LuaJIT is used to connect applications developed in LuaJIT to MariaDB and MySQL databases.
It works without a C module that you have to load; instead it utilizes the [FFI library](https://luajit.org/ext_ffi.html)
of LuaJIT to create a thin wrapper around the native C functions exported by the C library (libmariadb/libmysql),
giving the best possible performance. As such, its API is very near to that of the C library with no fancy abstractions
(but a few improvements to make it easier to work with it). Programmers already familiar with the C library or the old
[mysql module of PHP](https://www.php.net/manual/en/book.mysql.php) will have a good start with this library.
It supports both a procedural style just like the C API and an object-oriented style to make it a bit prettier.

### What is it like?

```lua
local mysql = require("mysql/mariadb");
local handler = mysql.connect("localhost", "user", "password", "database");

local result = handler:query("SELECT 5, NULL, 'Hello world'");
-- or with the procedural approach:
local result = mysql.query(handler, "SELECT 5, NULL, 'Hello world'");

local row = result:fetch_row();
-- row = {5, nil, "Hello world"}

-- Similar to prepared statements, you can use formatted SQL statements that will automatically convert
-- Lua types to the corresponding SQL types and are safe against SQL injection:
local query = handler:format("INSERT INTO `users`(`name`, `is_admin`, `email`) VALUES (?, ?, ?)", "O'Brian", true, nil);
-- query = "INSERT INTO `users`(`name`, `is_admin`, `email`) VALUES ('O\'Brian', TRUE, NULL)"
handler:query(query);
-- or simpler:
handler:queryf("INSERT INTO `users`(`name`, `is_admin`, `email`) VALUES (?, ?, ?)", "O'Brian", true, nil);
```

### How can I integrate it?
If your project uses Git, it's recommended to fetch it as a submodule in a folder named mysql.
Then you can include it very nicely with `require("mysql/mariadb")`. Of course you have to have installed the libmariadb
at a place where FFI can find it, e.g. in Windows the current directory.
