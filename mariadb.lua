--[[ Copyright (C) 2021-2024 Sabrosa
     This file is part of MariaDB Connector/LuaJIT.

     MariaDB Connector/LuaJIT is free software; you can redistribute it and/or
     modify it under the terms of the GNU Lesser General Public License as
     published by the Free Software Foundation; either version 2.1 of the
     License, or (at your option) any later version.

     MariaDB Connector/LuaJIT is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
     Lesser General Public License for more details.

     You should have received a copy of the GNU Lesser General Public
     License along with MariaDB Connector/LuaJIT; if not, see
     <https://www.gnu.org/licenses/>.
--]]


local io = require("io");
local string = require("string");
local ffi = require("ffi");
local band = require("bit").band;
--[[ Compatibility with LuaJIT 2.0
local table_new = require("table.new");
--]]
local debug_getinfo = require("debug").getinfo;
local table_pack = require("table").pack;
local ffi_string = ffi.string;
local tonumber = tonumber;
local NULL = nil; ---@type ffi.cdata* NULL pointer

do
	local current_dir = string.match(..., ".*[./\\]") or "";
	local file = io.open(current_dir .. "mariadb.h");
	assert(file, "Could not load mariadb.h!");
	ffi.cdef(file:read("*all"));
	file:close();
end

local mariadb;
if ffi.os == "Windows" then
	mariadb = ffi.load("libmariadb.dll");
else
	mariadb = ffi.load("libmariadb.so.3");
end

ffi.cdef(
[[
struct mysql_handle
{
	MYSQL *mysql;
};

struct mysql_result
{
	MYSQL_RES *res;
};
]]);

local mysql_handle;
local mysql_result;

---@return integer? level
local function blame_external_code()
	local level = 2;
	local this_src = debug_getinfo(1, "S").short_src;
	while true do
		local info = debug_getinfo(level, "S");
		if not info then
			return nil;
		end
		if info.short_src ~= this_src and info.what ~= "C" then
			return level - 1; -- minus the current function
		end
		level = level + 1;
	end
end

local function check_handle(handle)
	if not handle or handle.mysql == NULL then
		error("Expected a valid mysql_handle", blame_external_code());
	end
	return handle.mysql;
end

---@param handle mysql_handle
---@param str string
local function mysql_real_escape_string(handle, str)
	handle = check_handle(handle);
	local buf = ffi.new("char[?]", #str * 2 + 1);
	local len = tonumber(mariadb.mysql_real_escape_string(handle, buf, str, #str));
	return ffi_string(buf, len);
end

---@param handle mysql_handle
---@param param string|number|boolean|nil|(string|number|boolean)[]
---@param param_index integer
---@param table_index? integer
---@return string
local function format_parameter(handle, param, param_index, table_index)
	local param_type = type(param);
	if param_type == "string" then
		return "'" .. mysql_real_escape_string(handle, param) .. "'";
	elseif param_type == "number" then
		return tostring(param);
	elseif param_type == "boolean" then
		return tostring(param):upper();
	elseif param_type == "nil" then
		return "NULL";
	elseif param_type == "table" and not table_index then -- no tables in tables
		local str = "";
		for i, value in ipairs(param) do
			str = str .. format_parameter(handle, value, param_index, i);
			if i < #param then
				str = str .. ", ";
			end
		end
		return '(' .. str .. ')';
	else
		local err = "Bad argument to placeholder #" .. param_index;
		if table_index then
			err = err .. " (invalid type (" .. param_type .. ") at table index #" .. table_index .. ", expected string|number|boolean)";
		else
			err = err .. " (string|number|boolean|nil|table expected, got " .. param_type .. ")";
		end
		error(err, blame_external_code());
	end
end

--- Formats an SQL statement. Use `?` in the statement to indicate a placeholder that will be replaced with the corresponding parameter.
--- <br/>To write a literal `?`, escape it with a preceding backslash, i.e. `\?`.
---
--- The parameters will be formatted according to the following rules:
--- - strings are enclosed in single quotes and automatically escaped, i.e. `text with 'quotes'` => `'text with \'quotes\''`
--- - numbers are written as is
--- - booleans are converted to `TRUE` or `FALSE` (synonyms for 1 and 0)
--- - `nil` is converted to `NULL`
--- - arrays (!) are enclosed in parentheses and the formatted elements are separated by commas. Arrays in arrays are not supported.
--- <br/>For example, `{"a'b", 1, false}` => `('a\'b', 1, FALSE)`.
--- <br/>Note that arrays in Lua cannot contain `nil`.
---@param handle mysql_handle
---@param query string
---@param ... string|number|boolean|nil|(string|number|boolean)[]
---@return string
local function mysql_format(handle, query, ...)
	check_handle(handle); -- don't extract the handle
	local args = table_pack(...);
	local i = 0;
	query = query:gsub("(.)%?", function(prev_char)
		if prev_char == "\\" then
			return "?";
		end
		i = i + 1;
		if i > args.n then
			error("Bad argument to placeholder #" .. i .. " (no value)", blame_external_code());
		end
		return prev_char .. format_parameter(handle, args[i], i);
	end);
	return query;
end

---@param handle mysql_handle
---@param query string
---@return mysql_result?
local function mysql_buffered_query(handle, query)
	handle = check_handle(handle);
	if mariadb.mysql_real_query(handle, query, #query) ~= 0 then
		error("SQL: (" .. tonumber(mariadb.mysql_errno(handle)) .. ") " .. ffi_string(mariadb.mysql_error(handle)), blame_external_code());
	end
	local result = mariadb.mysql_store_result(handle);
	if result == NULL then
		if tonumber(mariadb.mysql_field_count(handle)) > 0 then
			error("SQL: (" .. tonumber(mariadb.mysql_errno(handle)) .. ") " .. ffi_string(mariadb.mysql_error(handle)), blame_external_code());
		end
		return nil;
	end
	return mysql_result(result);
end

---@class mysql_handle
---@field mysql ffi.cdata*
local mysql_handle_methods =
{
	---@param handle mysql_handle
	---@return boolean
	optionsv = function(handle, option, arg, ...)
		handle = check_handle(handle);
		return (tonumber(mariadb.mysql_optionsv(handle, option, arg, ...)) == 0);
	end,

	---@param handle mysql_handle
	---@param hostname? string
	---@param username string
	---@param password? string
	---@param database? string
	---@param port? integer
	---@param unix_socket? string
	---@param flags? integer
	real_connect = function(handle, hostname, username, password, database, port, unix_socket, flags)
		handle = check_handle(handle);
		port = port or 3306;
		flags = flags or 0;
		if mariadb.mysql_real_connect(handle, hostname, username, password, database, port, unix_socket, flags) == NULL then
			error("Unable to connect to MySQL: (" .. tonumber(mariadb.mysql_errno(handle)) .. ") " .. ffi_string(mariadb.mysql_error(handle)),
				blame_external_code());
		end
	end,

	---@param handle mysql_handle
	close = function(handle)
		if not handle then
			error("Expected a valid mysql_handle", blame_external_code());
		end
		if handle.mysql ~= NULL then
			mariadb.mysql_close(handle.mysql);
			handle.mysql = NULL;
		end
	end,
	real_escape_string = mysql_real_escape_string,
	escape_string = mysql_real_escape_string,
	format = mysql_format,
	real_query = mysql_buffered_query,
	query = mysql_buffered_query,

	--- Performs a formatted, buffered query. It's equivalent to calling `query()` with the result of `format()`.
	--- <br/>Specifications about the format of the query can be found in the documentation for `format()`.
	---@param handle mysql_handle
	---@param query string
	---@param ... string|number|boolean|nil|(string|number|boolean)[]
	---@return mysql_result?
	queryf = function(handle, query, ...)
		check_handle(handle);
		return handle:query(handle:format(query, ...));
	end,

	---@param handle mysql_handle
	---@param query string
	---@return mysql_result?
	unbuffered_query = function(handle, query)
		handle = check_handle(handle);
		if mariadb.mysql_real_query(handle, query, #query) ~= 0 then
			error("SQL: (" .. tonumber(mariadb.mysql_errno(handle)) .. ") " .. ffi_string(mariadb.mysql_error(handle)), blame_external_code());
		end
		local result = mariadb.mysql_use_result(handle);
		if result == NULL then
			if tonumber(mariadb.mysql_field_count(handle)) > 0 then
				error("SQL: (" .. tonumber(mariadb.mysql_errno(handle)) .. ") " .. ffi_string(mariadb.mysql_error(handle)), blame_external_code());
			end
			return nil;
		end
		return mysql_result(result);
	end,

	--- Performs a formatted, unbuffered query. It's equivalent to calling `unbuffered_query()` with the result of `format()`.
	--- <br/>Specifications about the format of the query can be found in the documentation for `format()`.
	---@param handle mysql_handle
	---@param query string
	---@param ... string|number|boolean|nil|(string|number|boolean)[]
	---@return mysql_result?
	unbuffered_queryf = function(handle, query, ...)
		check_handle(handle);
		return handle:unbuffered_query(handle:format(query, ...));
	end,

	---@param handle mysql_handle
	next_result = function(handle)
		handle = check_handle(handle);
		return (tonumber(mariadb.mysql_next_result(handle)) == 0);
	end,

	---@param handle mysql_handle
	ping = function(handle)
		handle = check_handle(handle);
		return (tonumber(mariadb.mysql_ping(handle)) == 0);
	end,

	---@param handle mysql_handle
	errno = function(handle)
		handle = check_handle(handle);
		return tonumber(mariadb.mysql_errno(handle));
	end,

	---@param handle mysql_handle
	error = function(handle)
		handle = check_handle(handle);
		return ffi_string(mariadb.mysql_error(handle));
	end,

	---@param handle mysql_handle
	---@param database string
	select_db = function(handle, database)
		handle = check_handle(handle);
		return (tonumber(mariadb.mysql_select_db(handle, database)) == 0);
	end,

	---@param handle mysql_handle
	affected_rows = function(handle)
		handle = check_handle(handle);
		return tonumber(mariadb.mysql_affected_rows(handle));
	end,

	---@param handle mysql_handle
	---@param username string
	---@param password? string
	---@param database? string
	change_user = function(handle, username, password, database)
		handle = check_handle(handle);
		return (tonumber(mariadb.mysql_change_user(handle, username, password, database)) == 0);
	end,

	---@param handle mysql_handle
	get_character_set_info = function(handle)
		handle = check_handle(handle);
		local charset_info = ffi.new("MY_CHARSET_INFO[1]");
		mariadb.mysql_get_character_set_info(handle, charset_info);
		return
		{
			number = tonumber(charset_info[0].number),
			state = tonumber(charset_info[0].state),
			csname = (charset_info[0].csname ~= NULL and ffi_string(charset_info[0].csname)),
			name = (charset_info[0].name ~= NULL and ffi_string(charset_info[0].name)),
			comment = (charset_info[0].comment ~= NULL and ffi_string(charset_info[0].comment)),
			dir = (charset_info[0].dir ~= NULL and ffi_string(charset_info[0].dir)),
			mbminlen = tonumber(charset_info[0].mbminlen),
			mbmaxlen = tonumber(charset_info[0].mbmaxlen)
		};
	end,

	---@param handle mysql_handle
	---@param charset string
	set_character_set = function(handle, charset)
		handle = check_handle(handle);
		return (tonumber(mariadb.mysql_set_character_set(handle, charset)) == 0);
	end,

	---@param handle mysql_handle
	get_host_info = function(handle)
		handle = check_handle(handle);
		return ffi_string(mariadb.mysql_get_host_info(handle));
	end,

	---@param handle mysql_handle
	get_proto_info = function(handle)
		handle = check_handle(handle);
		return tonumber(mariadb.mysql_get_proto_info(handle));
	end,

	---@param handle mysql_handle
	get_server_version = function(handle)
		handle = check_handle(handle);
		return tonumber(mariadb.mysql_get_server_version(handle));
	end,

	---@param handle mysql_handle
	get_server_info = function(handle)
		handle = check_handle(handle);
		return ffi_string(mariadb.mysql_get_server_info(handle));
	end,

	---@param handle mysql_handle
	info = function(handle)
		handle = check_handle(handle);
		local info = mariadb.mysql_info(handle);
		if info ~= NULL then
			return ffi_string(info);
		end
		return nil;
	end,

	---@param handle mysql_handle
	stat = function(handle)
		handle = check_handle(handle);
		local stat = mariadb.mysql_stat(handle);
		if stat ~= NULL then
			return ffi_string(stat);
		end
		return nil;
	end,

	---@param handle mysql_handle
	warning_count = function(handle)
		handle = check_handle(handle);
		return tonumber(mariadb.mysql_warning_count(handle));
	end,

	---@param handle mysql_handle
	insert_id = function(handle)
		handle = check_handle(handle);
		return tonumber(mariadb.mysql_insert_id(handle));
	end
};
---@type mysql_handle
mysql_handle = ffi.metatype("struct mysql_handle",
{
	__index = mysql_handle_methods,
	__gc = mysql_handle_methods.close
});


local function check_result(result)
	if not result then
		error("Expected a valid mysql_result", blame_external_code());
	end
	return result.res;
end

---@return (string|number|nil)[]?
local function push_row(result)
	local row = mariadb.mysql_fetch_row(result);
	if row ~= NULL then
		local fields = mariadb.mysql_fetch_fields(result);
		local num_fields = tonumber(mariadb.mysql_num_fields(result));
		local data = {}; --table_new(num_fields, 0);
		for i = 0, num_fields - 1 do
			if row[i] ~= NULL then
				local value = ffi_string(row[i]);
				local type = tonumber(fields[i].type);
				if (type <= mariadb.MYSQL_TYPE_INT24 and type ~= mariadb.MYSQL_TYPE_TIMESTAMP)
				or type == mariadb.MYSQL_TYPE_YEAR or type == mariadb.MYSQL_TYPE_NEWDECIMAL then
					data[i + 1] = tonumber(value);
				else
					data[i + 1] = value;
				end
			end
		end
		return data;
	end
	return nil;
end

---@return table<string, string|number|nil>?
local function push_row_assoc(result)
	local row = mariadb.mysql_fetch_row(result);
	if row ~= NULL then
		local fields = mariadb.mysql_fetch_fields(result);
		local num_fields = tonumber(mariadb.mysql_num_fields(result));
		local data = {}; --table_new(0, num_fields);
		for i = 0, num_fields - 1 do
			if row[i] ~= NULL then
				local value = ffi_string(row[i]);
				local type = tonumber(fields[i].type);
				if (type <= mariadb.MYSQL_TYPE_INT24 and type ~= mariadb.MYSQL_TYPE_TIMESTAMP)
				or type == mariadb.MYSQL_TYPE_YEAR or type == mariadb.MYSQL_TYPE_NEWDECIMAL then
					data[ffi_string(fields[i].name, tonumber(fields[i].name_length))] = tonumber(value);
				else
					data[ffi_string(fields[i].name, tonumber(fields[i].name_length))] = value;
				end
			end
		end
		return data;
	end
	return nil;
end

local function push_field(result)
	local field = mariadb.mysql_fetch_field(result);
	if field == NULL then
		return nil;
	end
	local flags = tonumber(field.flags);
	return
	{
		name = (field.name ~= NULL and ffi_string(field.name, tonumber(field.name_length))),
		org_name = (field.org_name ~= NULL and ffi_string(field.org_name, tonumber(field.org_name_length))),
		table = (field.table ~= NULL and ffi_string(field.table, tonumber(field.table_length))),
		org_table = (field.org_table ~= NULL and ffi_string(field.org_table, tonumber(field.org_table_length))),
		db = (field.db ~= NULL and ffi_string(field.db, tonumber(field.db_length))),
		def = (field.def ~= NULL and ffi_string(field.def, tonumber(field.def_length))),
		length = tonumber(field.length),
		max_length = tonumber(field.max_length),
		decimals = tonumber(field.decimals),
		charsetnr = tonumber(field.charsetnr),
		type = tonumber(field.type),
		not_null = band(flags, mariadb.NOT_NULL_FLAG) ~= 0,
		primary_key = band(flags, mariadb.PRI_KEY_FLAG) ~= 0,
		unique_key = band(flags, mariadb.UNIQUE_KEY_FLAG) ~= 0,
		multiple_key = band(flags, mariadb.MULTIPLE_KEY_FLAG) ~= 0,
		blob = band(flags, mariadb.BLOB_FLAG) ~= 0,
		unsigned = band(flags, mariadb.UNSIGNED_FLAG) ~= 0,
		zerofill = band(flags, mariadb.ZEROFILL_FLAG) ~= 0,
		binary = band(flags, mariadb.BINARY_FLAG) ~= 0,
		enum = band(flags, mariadb.ENUM_FLAG) ~= 0,
		auto_increment = band(flags, mariadb.AUTO_INCREMENT_FLAG) ~= 0,
		timestamp = band(flags, mariadb.TIMESTAMP_FLAG) ~= 0,
		set = band(flags, mariadb.SET_FLAG) ~= 0,
		no_default_value = band(flags, mariadb.NO_DEFAULT_VALUE_FLAG) ~= 0,
		on_update_now = band(flags, mariadb.ON_UPDATE_NOW_FLAG) ~= 0,
		numeric = band(flags, mariadb.NUM_FLAG) ~= 0
	};
end

---@class mysql_result
local mysql_result_methods =
{
	---@param result mysql_result
	num_fields = function(result)
		result = check_result(result);
		return tonumber(mariadb.mysql_num_fields(result));
	end,

	---@param result mysql_result
	num_rows = function(result)
		result = check_result(result);
		return tonumber(mariadb.mysql_num_rows(result));
	end,

	---@param result mysql_result
	---@param offset integer
	data_seek = function(result, offset)
		result = check_result(result);
		mariadb.mysql_data_seek(result, offset - 1);
	end,

	---@param result mysql_result
	fetch_row = function(result)
		result = check_result(result);
		return push_row(result);
	end,

	---@param result mysql_result
	rows = function(result)
		result = check_result(result);
		return push_row, result;
	end,

	---@param result mysql_result
	fetch_assoc = function(result)
		result = check_result(result);
		return push_row_assoc(result);
	end,

	---@param result mysql_result
	rows_assoc = function(result)
		result = check_result(result);
		return push_row_assoc, result;
	end,

	---@param result mysql_result
	---@param row_offset integer
	---@param field_offset integer
	result = function(result, row_offset, field_offset)
		result = check_result(result);
		row_offset = row_offset - 1;
		field_offset = field_offset - 1;

		mariadb.mysql_data_seek(result, row_offset);
		local row = mariadb.mysql_fetch_row(result);
		if row ~= NULL and field_offset >= 0 and field_offset < mariadb.mysql_num_fields(result) and row[field_offset] ~= NULL then
			local value = ffi_string(row[field_offset]);
			local type = tonumber(mariadb.mysql_fetch_field_direct(result, field_offset).type);
			if (type <= mariadb.MYSQL_TYPE_INT24 and type ~= mariadb.MYSQL_TYPE_TIMESTAMP)
			or type == mariadb.MYSQL_TYPE_YEAR or type == mariadb.MYSQL_TYPE_NEWDECIMAL then
				return tonumber(value);
			else
				return value;
			end
		end
		return nil;
	end,

	---@param result mysql_result
	---@param offset integer
	fetch_field = function(result, offset)
		result = check_result(result);
		if offset then
			mariadb.mysql_field_seek(result, offset - 1);
		end
		return push_field(result);
	end,

	---@param result mysql_result
	fields = function(result)
		result = check_result(result);
		return push_field, result;
	end,

	---@param result mysql_result
	---@param offset integer
	field_name = function(result, offset)
		result = check_result(result);
		local field = mariadb.mysql_fetch_field_direct(result, offset - 1);
		if field ~= NULL then
			return ffi_string(field.name, tonumber(field.name_length));
		else
			return nil;
		end
	end
};
---@type mysql_result
mysql_result = ffi.metatype("struct mysql_result",
{
	__index = mysql_result_methods,
	__gc = function(result)
		mariadb.mysql_free_result(result.res);
	end
});

return
{
	-- flags for real_connect()
	CLIENT_FOUND_ROWS = mariadb.CLIENT_FOUND_ROWS,
	CLIENT_NO_SCHEMA = mariadb.CLIENT_NO_SCHEMA,
	CLIENT_COMPRESS = mariadb.CLIENT_COMPRESS,
	CLIENT_IGNORE_SPACE = mariadb.CLIENT_IGNORE_SPACE,
	CLIENT_LOCAL_FILES = mariadb.CLIENT_LOCAL_FILES,
	CLIENT_MULTI_STATEMENTS = mariadb.CLIENT_MULTI_STATEMENTS,
	CLIENT_MULTI_RESULTS = mariadb.CLIENT_MULTI_RESULTS,
	-- field types
	TYPE_DECIMAL = mariadb.MYSQL_TYPE_DECIMAL,
	TYPE_TINY = mariadb.MYSQL_TYPE_TINY,
	TYPE_CHAR = mariadb.FIELD_TYPE_TINY,
	TYPE_SHORT = mariadb.MYSQL_TYPE_SHORT,
	TYPE_LONG = mariadb.MYSQL_TYPE_LONG,
	TYPE_FLOAT = mariadb.MYSQL_TYPE_FLOAT,
	TYPE_DOUBLE = mariadb.MYSQL_TYPE_DOUBLE,
	TYPE_NULL = mariadb.MYSQL_TYPE_NULL,
	TYPE_TIMESTAMP = mariadb.MYSQL_TYPE_TIMESTAMP,
	TYPE_LONGLONG = mariadb.MYSQL_TYPE_LONGLONG,
	TYPE_INT24 = mariadb.MYSQL_TYPE_INT24,
	TYPE_DATE = mariadb.MYSQL_TYPE_DATE,
	TYPE_TIME = mariadb.MYSQL_TYPE_TIME,
	TYPE_DATETIME = mariadb.MYSQL_TYPE_DATETIME,
	TYPE_YEAR = mariadb.MYSQL_TYPE_YEAR,
	TYPE_NEWDATE = mariadb.MYSQL_TYPE_NEWDATE,
	TYPE_VARCHAR = mariadb.MYSQL_TYPE_VARCHAR,
	TYPE_BIT = mariadb.MYSQL_TYPE_BIT,
	TYPE_TIMESTAMP2 = mariadb.MYSQL_TYPE_TIMESTAMP2,
	TYPE_DATETIME2 = mariadb.MYSQL_TYPE_DATETIME2,
	TYPE_TIME2 = mariadb.MYSQL_TYPE_TIME2,
	TYPE_JSON = mariadb.MYSQL_TYPE_JSON,
	TYPE_NEWDECIMAL = mariadb.MYSQL_TYPE_NEWDECIMAL,
	TYPE_ENUM = mariadb.MYSQL_TYPE_ENUM,
	TYPE_INTERVAL = mariadb.MYSQL_TYPE_ENUM,
	TYPE_SET = mariadb.MYSQL_TYPE_SET,
	TYPE_TINY_BLOB = mariadb.MYSQL_TYPE_TINY_BLOB,
	TYPE_MEDIUM_BLOB = mariadb.MYSQL_TYPE_MEDIUM_BLOB,
	TYPE_LONG_BLOB = mariadb.MYSQL_TYPE_LONG_BLOB,
	TYPE_BLOB = mariadb.MYSQL_TYPE_BLOB,
	TYPE_VAR_STRING = mariadb.MYSQL_TYPE_VAR_STRING,
	TYPE_STRING = mariadb.MYSQL_TYPE_STRING,
	TYPE_GEOMETRY = mariadb.MYSQL_TYPE_GEOMETRY,
	MAX_NO_FIELD_TYPES = mariadb.MAX_NO_FIELD_TYPES,
	-- options for optionsv()
	INIT_COMMAND = mariadb.MYSQL_INIT_COMMAND,
	OPT_CONNECT_TIMEOUT = mariadb.MYSQL_OPT_CONNECT_TIMEOUT,
	PROGRESS_CALLBACK = mariadb.MYSQL_PROGRESS_CALLBACK,
	OPT_RECONNECT = mariadb.MYSQL_OPT_RECONNECT,
	OPT_READ_TIMEOUT = mariadb.MYSQL_OPT_READ_TIMEOUT,
	OPT_WRITE_TIMEOUT = mariadb.MYSQL_OPT_WRITE_TIMEOUT,
	REPORT_DATA_TRUNCATION = mariadb.MYSQL_REPORT_DATA_TRUNCATION,
	SET_CHARSET_DIR = mariadb.MYSQL_SET_CHARSET_DIR,
	SET_CHARSET_NAME = mariadb.MYSQL_SET_CHARSET_NAME,
	OPT_BIND = mariadb.MYSQL_OPT_BIND,
	OPT_NONBLOCK = mariadb.MYSQL_OPT_NONBLOCK,
	OPT_CAN_HANDLE_EXPIRED_PASSWORDS = mariadb.MYSQL_OPT_CAN_HANDLE_EXPIRED_PASSWORDS,
	OPT_MAX_ALLOWED_PACKET = mariadb.MYSQL_OPT_MAX_ALLOWED_PACKET,
	OPT_NET_BUFFER_LENGTH = mariadb.MYSQL_OPT_NET_BUFFER_LENGTH,
	OPT_HOST = mariadb.MARIADB_OPT_HOST,
	OPT_USER = mariadb.MARIADB_OPT_USER,
	OPT_PASSWORD = mariadb.MARIADB_OPT_PASSWORD,
	OPT_SCHEMA = mariadb.MARIADB_OPT_SCHEMA,
	OPT_PORT = mariadb.MARIADB_OPT_PORT,
	OPT_UNIXSOCKET = mariadb.MARIADB_OPT_UNIXSOCKET,
	OPT_NAMED_PIPE = mariadb.MYSQL_OPT_NAMED_PIPE,
	OPT_PROTOCOL = mariadb.MYSQL_OPT_PROTOCOL,
	OPT_FOUND_ROWS = mariadb.MARIADB_OPT_FOUND_ROWS,
	OPT_COMPRESS = mariadb.MYSQL_OPT_COMPRESS,
	OPT_LOCAL_INFILE = mariadb.MYSQL_OPT_LOCAL_INFILE,
	OPT_MULTI_STATEMENTS = mariadb.MARIADB_OPT_MULTI_STATEMENTS,
	OPT_MULTI_RESULTS = mariadb.MARIADB_OPT_MULTI_RESULTS,
	SHARED_MEMORY_BASE_NAME = mariadb.MYSQL_SHARED_MEMORY_BASE_NAME,
	OPT_SSL_KEY = mariadb.MYSQL_OPT_SSL_KEY,
	OPT_SSL_CERT = mariadb.MYSQL_OPT_SSL_CERT,
	OPT_SSL_CA = mariadb.MYSQL_OPT_SSL_CA,
	OPT_SSL_CAPATH = mariadb.MYSQL_OPT_SSL_CAPATH,
	OPT_SSL_CIPHER = mariadb.MYSQL_OPT_SSL_CIPHER,
	OPT_SSL_CRL = mariadb.MYSQL_OPT_SSL_CRL,
	OPT_SSL_CRLPATH = mariadb.MYSQL_OPT_SSL_CRLPATH,
	OPT_SSL_FP = mariadb.MARIADB_OPT_SSL_FP,
	OPT_TLS_PEER_FP = mariadb.MARIADB_OPT_TLS_PEER_FP,
	OPT_SSL_FP_LIST = mariadb.MARIADB_OPT_SSL_FP_LIST,
	OPT_TLS_PEER_FP_LIST = mariadb.MARIADB_OPT_TLS_PEER_FP_LIST,
	OPT_TLS_PASSPHRASE = mariadb.MARIADB_OPT_TLS_PASSPHRASE,
	OPT_TLS_VERSION = mariadb.MARIADB_OPT_TLS_VERSION,
	OPT_SSL_VERIFY_SERVER_CERT = mariadb.MYSQL_OPT_SSL_VERIFY_SERVER_CERT,
	OPT_SSL_ENFORCE = mariadb.MYSQL_OPT_SSL_ENFORCE,
	OPT_TLS_CIPHER_STRENGTH = mariadb.MARIADB_OPT_TLS_CIPHER_STRENGTH,
	DEFAULT_AUTH = mariadb.MYSQL_DEFAULT_AUTH,
	ENABLE_CLEARTEXT_PLUGIN = mariadb.MYSQL_ENABLE_CLEARTEXT_PLUGIN,
	OPT_CONNECTION_HANDLER = mariadb.MARIADB_OPT_CONNECTION_HANDLER,
	OPT_USERDATA = mariadb.MARIADB_OPT_USERDATA,
	OPT_CONNECTION_READ_ONLY = mariadb.MARIADB_OPT_CONNECTION_READ_ONLY,
	PLUGIN_DIR = mariadb.MYSQL_PLUGIN_DIR,
	SECURE_AUTH = mariadb.MYSQL_SECURE_AUTH,
	SERVER_PUBLIC_KEY = mariadb.MYSQL_SERVER_PUBLIC_KEY,
	READ_DEFAULT_FILE = mariadb.MYSQL_READ_DEFAULT_FILE,
	READ_DEFAULT_GROUP = mariadb.MYSQL_READ_DEFAULT_GROUP,
	OPT_CONNECT_ATTR_DELETE = mariadb.MYSQL_OPT_CONNECT_ATTR_DELETE,
	OPT_CONNECT_ATTR_ADD = mariadb.MYSQL_OPT_CONNECT_ATTR_ADD,
	OPT_CONNECT_ATTR_RESET = mariadb.MYSQL_OPT_CONNECT_ATTR_RESET,

	-- static functions

	---@return mysql_handle
	init = function()
		return mysql_handle(mariadb.mysql_init(NULL));
	end,

	---@param hostname? string
	---@param username string
	---@param password? string
	---@param database? string
	---@param port? integer
	---@param unix_socket? string
	---@param flags? integer
	---@return mysql_handle
	connect = function(hostname, username, password, database, port, unix_socket, flags)
		local handle = mysql_handle(mariadb.mysql_init(NULL));
		handle:real_connect(hostname, username, password, database, port, unix_socket, flags);
		return handle;
	end,

	get_client_version = function()
		return tonumber(mariadb.mysql_get_client_version());
	end,

	get_client_info = function()
		return ffi_string(mariadb.mysql_get_client_info());
	end,

	---@param str string
	hex_string = function(str)
		local buff = ffi.new("char[?]", #str * 2 + 1);
		local len = tonumber(mariadb.mysql_hex_string(buff, str, #str));
		return ffi_string(buff, len);
	end,

	-- handle methods
	optionsv = mysql_handle_methods.optionsv,
	real_connect = mysql_handle_methods.real_connect,
	close = mysql_handle_methods.close,
	ping = mysql_handle_methods.ping,
	errno = mysql_handle_methods.errno,
	error = mysql_handle_methods.error,
	select_db = mysql_handle_methods.select_db,
	real_escape_string = mysql_handle_methods.real_escape_string,
	escape_string = mysql_handle_methods.escape_string,
	format = mysql_handle_methods.format,
	affected_rows = mysql_handle_methods.affected_rows,
	change_user = mysql_handle_methods.change_user,
	get_character_set_info = mysql_handle_methods.get_character_set_info,
	set_character_set = mysql_handle_methods.set_character_set,
	get_host_info = mysql_handle_methods.get_host_info,
	get_proto_info = mysql_handle_methods.get_proto_info,
	get_server_version = mysql_handle_methods.get_server_version,
	get_server_info = mysql_handle_methods.get_server_info,
	info = mysql_handle_methods.info,
	stat = mysql_handle_methods.stat,
	warning_count = mysql_handle_methods.warning_count,
	insert_id = mysql_handle_methods.insert_id,
	real_query = mysql_handle_methods.real_query,
	query = mysql_handle_methods.query,
	queryf = mysql_handle_methods.queryf,
	unbuffered_query = mysql_handle_methods.unbuffered_query,
	unbuffered_queryf = mysql_handle_methods.unbuffered_queryf,
	next_result = mysql_handle_methods.next_result,
	-- result methods
	num_fields = mysql_result_methods.num_fields,
	num_rows = mysql_result_methods.num_rows,
	data_seek = mysql_result_methods.data_seek,
	fetch_row = mysql_result_methods.fetch_row,
	rows = mysql_result_methods.rows,
	fetch_assoc = mysql_result_methods.fetch_assoc,
	rows_assoc = mysql_result_methods.rows_assoc,
	result = mysql_result_methods.result,
	fetch_field = mysql_result_methods.fetch_field,
	fields = mysql_result_methods.fields,
	field_name = mysql_result_methods.field_name
};
